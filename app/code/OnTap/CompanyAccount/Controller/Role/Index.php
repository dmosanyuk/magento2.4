<?php
namespace OnTap\CompanyAccount\Controller\Role;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Result\PageFactory;
use OnTap\CompanyAccount\Helper\Data;
use OnTap\CompanyAccount\Helper\PermissionsChecker;
use OnTap\CompanyAccount\Model\Config\Source\Permissions;
use OnTap\Portal\Controller\AccountInterface;

/**
 * Class Index
 *
 * @package OnTap\CompanyAccount\Block\Role
 *
 * @SuppressWarnings(PHPMD.AllPurposeAction)
 */
class Index extends \Magento\Framework\App\Action\Action implements AccountInterface
{
    /**
     * Menu Item Id
     */
    const MENU_ITEM_ID = 'OnTap_CompanyAccount::company_manage_roles';

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Magento\Customer\Model\Url
     */
    private $url;

    /**
     * @var Data
     */
    private $helper;

    /**
     * @var PermissionsChecker
     */
    private $permissionsChecker;

    /**
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Customer\Model\Url $url
     * @param PageFactory $resultPageFactory
     * @param PermissionsChecker $permissionsChecker
     * @param Data $helper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Url $url,
        PageFactory $resultPageFactory,
        PermissionsChecker $permissionsChecker,
        Data $helper
    ) {
        $this->url = $url;
        $this->helper = $helper;
        $this->customerSession = $this->helper->getCustomerSession();
        $this->permissionsChecker = $permissionsChecker;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Check customer authentication
     *
     * @param RequestInterface $request
     * @return \Magento\Framework\App\ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function dispatch(RequestInterface $request)
    {
        $loginUrl = $this->url->getLoginUrl();

        if (!$this->customerSession->authenticate($loginUrl)) {
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
        }
        return parent::dispatch($request);
    }

    /**
     * Sub-user index
     *
     * @return bool|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\View\Result\Page
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        if (!$this->helper->isCompanyAccount() ||
            !$this->helper->isEnable($this->customerSession->getCustomer()->getWebsiteId())
        ) {
            return $this->resultRedirectFactory->create()
                ->setUrl($this->_redirect->getRefererUrl());
        }
        try {
            $checkVal = $this->permissionsChecker->check(Permissions::MANAGE_SUB_USER_AND_ROLES);
            if ($checkVal) {
                return $checkVal;
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }
        $resultPage = $this->resultPageFactory->create();
        if ($block = $this->_view->getLayout()->getBlock('ontap_companyaccount_role_index')) {
            $block->setRefererUrl($this->_redirect->getRefererUrl());
        }
        $resultPage->getConfig()->getTitle()->set(__('Manage Role and Permissions'));
        return $resultPage;
    }
}
