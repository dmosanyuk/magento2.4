<?php
/**
 * Copyright (c) On Tap Networks Limited.
 */

namespace OnTap\CompanyAccount\Controller\Company;

use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;
use Magento\Framework\View\Result\PageFactory;
use OnTap\Portal\Controller\AccountInterface;

/**
 * Class Index
 *
 * @package OnTap\CompanyAccount\Controller\Company
 */
class Index implements HttpGetActionInterface, AccountInterface
{
    /**
     * Menu Item Id
     */
    const MENU_ITEM_ID = 'OnTap_CompanyAccount::company_information';

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Index constructor.
     *
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        return $this->resultPageFactory->create();
    }
}
