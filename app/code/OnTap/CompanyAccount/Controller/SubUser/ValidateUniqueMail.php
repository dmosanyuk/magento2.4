<?php
declare(strict_types=1);
namespace OnTap\CompanyAccount\Controller\SubUser;

use OnTap\CompanyAccount\Api\SubUserRepositoryInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Exception\AlreadyExistsException;

/**
 * Class ValidateUniqueMail
 *
 * @package OnTap\CompanyAccount\Controller\SubUser
 *
 * @SuppressWarnings(PHPMD.AllPurposeAction)
 */
class ValidateUniqueMail extends \Magento\Framework\App\Action\Action
{
    /**
     * @var SubUserRepositoryInterface
     */
    private $subUserRepository;

    /**
     * @var JsonFactory
     */
    private $jsonFactory;

    /**
     * ValidateUniqueMail constructor.
     *
     * @param SubUserRepositoryInterface $subUserRepository
     * @param JsonFactory $jsonFactory
     * @param Context $context
     */
    public function __construct(
        SubUserRepositoryInterface $subUserRepository,
        JsonFactory $jsonFactory,
        Context $context
    ) {
        $this->subUserRepository = $subUserRepository;
        parent::__construct($context);
        $this->jsonFactory = $jsonFactory;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $customerId = $this->getRequest()->getParam('customer_id');
        $email = $this->getRequest()->getParam('sub_email');
        $subId = $this->getRequest()->getParam('sub_id');
        $resultJson = $this->jsonFactory->create();
        try {
            $this->subUserRepository->validateUniqueSubMail($customerId, $email, $subId);
            $resultJson->setData('true');
        } catch (AlreadyExistsException $e) {
            $resultJson->setData($e->getMessage());
        }
        return $resultJson;
    }
}
