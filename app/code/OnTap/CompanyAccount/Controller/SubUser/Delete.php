<?php
declare(strict_types=1);
namespace OnTap\CompanyAccount\Controller\SubUser;

use OnTap\CompanyAccount\Api\SubUserRepositoryInterface;
use OnTap\CompanyAccount\Helper\Data;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Psr\Log\LoggerInterface;

/**
 * Class Delete
 *
 * @package OnTap\CompanyAccount\Controller\SubUser
 *
 * @SuppressWarnings(PHPMD.AllPurposeAction)
 */
class Delete extends \Magento\Framework\App\Action\Action
{
    /**
     * @var SubUserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var \OnTap\CompanyAccount\Helper\EmailHelper
     */
    private $emailHelper;

    /**
     * @var Data
     */
    private $helper;

    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var JsonFactory
     */
    private $jsonFactory;

    /**
     * @var \OnTap\CompanyAccount\Helper\FormHelper
     */
    private $formHelper;

    /**
     * Delete constructor.
     *
     * @param SubUserRepositoryInterface $userRepository
     * @param LoggerInterface $logger
     * @param Data $helper
     * @param \OnTap\CompanyAccount\Helper\FormHelper $formHelper
     * @param \OnTap\CompanyAccount\Helper\EmailHelper $emailHelper
     * @param JsonFactory $jsonFactory
     * @param Context $context
     */
    public function __construct(
        SubUserRepositoryInterface $userRepository,
        LoggerInterface $logger,
        Data $helper,
        \OnTap\CompanyAccount\Helper\FormHelper $formHelper,
        \OnTap\CompanyAccount\Helper\EmailHelper $emailHelper,
        JsonFactory $jsonFactory,
        Context $context
    ) {
        $this->userRepository = $userRepository;
        $this->logger = $logger;
        $this->emailHelper = $emailHelper;
        $this->helper = $helper;
        $this->customerSession = $this->helper->getCustomerSession();
        $this->jsonFactory = $jsonFactory;
        $this->formHelper = $formHelper;
        parent::__construct($context);
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        if (!$this->formHelper->validate($this->getRequest())) {
            return $this->jsonFactory->create()->setData(['remove_row' => false]);
        }
        $removeRow = true;
        if (!$this->helper->isCompanyAccount() ||
            !$this->helper->isEnable($this->customerSession->getCustomer()->getWebsiteId())
        ) {
            return $this->resultRedirectFactory->create()
                ->setPath('customer/account/');
        }
        try {
            if ($this->getRequest()->isPost()) {
                $subId = $this->getRequest()->getParam('sub_id');
                $this->emailHelper->sendRemoveNotificationMailToSubUser(null, (int) $subId);
                $this->userRepository->deleteById((int) $subId);

                $this->messageManager->addSuccessMessage(__('You deleted the user.'));
            }
        } catch (\Exception $e) {
            $removeRow = false;
            $this->messageManager->addErrorMessage(__('We can\'t delete the user right now.'));
            $this->logger->critical($e);
        }

        return $this->jsonFactory->create()
            ->setData(
                ['remove_row' => $removeRow]
            );
    }
}
