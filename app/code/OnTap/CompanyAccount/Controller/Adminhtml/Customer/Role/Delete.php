<?php
declare(strict_types=1);
namespace OnTap\CompanyAccount\Controller\Adminhtml\Customer\Role;

use OnTap\CompanyAccount\Api\SubRoleRepositoryInterface;
use OnTap\CompanyAccount\Exception\CantDeleteAssignedRole;
use OnTap\CompanyAccount\Helper\ActionHelper;
use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Psr\Log\LoggerInterface;

/**
 * Class Delete
 *
 * @package OnTap\CompanyAccount\Controller\Adminhtml\customer\Role
 */
class Delete extends Action implements HttpPostActionInterface
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_ROLE_DELETE = 'OnTap_CompanyAccount::role_delete';

    /**
     * @var SubRoleRepositoryInterface
     */
    private $roleRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var ActionHelper
     */
    private $actionHelper;

    /**
     * Delete constructor.
     *
     * @param SubRoleRepositoryInterface $roleRepository
     * @param LoggerInterface $logger
     * @param ActionHelper $actionHelper
     * @param JsonFactory $resultJsonFactory
     * @param Action\Context $context
     */
    public function __construct(
        SubRoleRepositoryInterface $roleRepository,
        LoggerInterface $logger,
        ActionHelper $actionHelper,
        JsonFactory $resultJsonFactory,
        Action\Context $context
    ) {
        $this->roleRepository = $roleRepository;
        $this->logger = $logger;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->actionHelper = $actionHelper;
        parent::__construct($context);
    }

    /**
     * @inheritDoc
     */
    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Delete role action
     */
    public function execute()
    {
        $error = false;
        if ($this->_authorization->isAllowed(self::ADMIN_ROLE_DELETE)) {
            try {
                $roleId = $this->getRequest()->getParam('id');
                $message = $this->actionHelper->destroyRole($this->roleRepository, $roleId);
            } catch (CantDeleteAssignedRole $e) {
                $error = true;
                $message = $e->getMessage();
            } catch (\Exception $e) {
                $error = true;
                $message = __('We can\'t delete the role right now.');
                $this->logger->critical($e);
            }
        } else {
            $error = true;
            $message = __('Sorry, you need permissions to %1.', __('delete role'));
        }
        $resultJson = $this->resultJsonFactory->create();
        $resultJson->setData(
            [
                'message' => $message,
                'error' => $error,
            ]
        );

        return $resultJson;
    }
}
