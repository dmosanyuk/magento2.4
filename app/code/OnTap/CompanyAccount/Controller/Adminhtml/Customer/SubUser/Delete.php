<?php
declare(strict_types=1);
namespace OnTap\CompanyAccount\Controller\Adminhtml\Customer\SubUser;

use OnTap\CompanyAccount\Api\SubUserRepositoryInterface;
use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Psr\Log\LoggerInterface;

/**
 * Class Delete
 *
 * @package OnTap\CompanyAccount\Controller\Adminhtml\customer\Role
 */
class Delete extends Action implements HttpPostActionInterface
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_SUB_USER_DELETE = 'OnTap_CompanyAccount::sub_user_delete';

    /**
     * @var SubUserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var \OnTap\CompanyAccount\Helper\EmailHelper
     */
    private $emailHelper;

    /**
     * Delete constructor.
     *
     * @param SubUserRepositoryInterface $userRepository
     * @param LoggerInterface $logger
     * @param JsonFactory $resultJsonFactory
     * @param \OnTap\CompanyAccount\Helper\EmailHelper $emailHelper
     * @param Action\Context $context
     */
    public function __construct(
        SubUserRepositoryInterface $userRepository,
        LoggerInterface $logger,
        JsonFactory $resultJsonFactory,
        \OnTap\CompanyAccount\Helper\EmailHelper $emailHelper,
        Action\Context $context
    ) {
        $this->userRepository = $userRepository;
        $this->logger = $logger;
        $this->emailHelper = $emailHelper;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * @inheritDoc
     */
    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Delete sub-user action
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $error = false;
        if ($this->_authorization->isAllowed(self::ADMIN_SUB_USER_DELETE)) {
            try {
                $subId = $this->getRequest()->getParam('id');
                $customerId = $this->getRequest()->getParam('customer_id');
                $this->emailHelper->sendRemoveNotificationMailToSubUser((int) $customerId, (int) $subId);
                $this->userRepository->deleteById((int) $subId);
                $message = __('You deleted the sub-user.');
            } catch (\Exception $e) {
                $error = true;
                $message = __('We can\'t delete the sub-user right now.');
                $this->logger->critical($e);
            }
        } else {
            $error = true;
            $message = __('Sorry, you need permissions to %1.', __('delete sub-user'));
        }
        $resultJson = $this->resultJsonFactory->create();
        $resultJson->setData(
            [
                'message' => $message,
                'error' => $error,
            ]
        );

        return $resultJson;
    }
}
