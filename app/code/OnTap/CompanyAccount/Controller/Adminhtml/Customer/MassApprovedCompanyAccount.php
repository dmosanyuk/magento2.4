<?php
namespace OnTap\CompanyAccount\Controller\Adminhtml\Customer;

use OnTap\CompanyAccount\Model\Config\Source\CompanyAccountValue;

/**
 * Class MassApprovedCompanyAccount
 *
 * @package OnTap\CompanyAccount\Controller\Adminhtml\System\Config
 *
 * @SuppressWarnings(PHPMD.AllPurposeAction)
 */
class MassApprovedCompanyAccount extends MassActionCompanyAccountAbstract
{
    /**
     * @inheritDoc
     */
    protected function setCompanyStatusVal()
    {
        return $this->statusValue = CompanyAccountValue::IS_COMPANY_ACCOUNT;
    }
}
