<?php
namespace OnTap\CompanyAccount\Controller\Adminhtml\Customer;

use OnTap\CompanyAccount\Model\Config\Source\CompanyAccountValue;

/**
 * Class MassApprovedCompanyAccount
 *
 * @package OnTap\CompanyAccount\Controller\Adminhtml\System\Config
 *
 * @SuppressWarnings(PHPMD.AllPurposeAction)
 */
class MassDisapprovedCompanyAccount extends MassActionCompanyAccountAbstract
{
    /**
     * Set status value for action
     *
     * @return int
     */
    protected function setCompanyStatusVal()
    {
        return $this->statusValue = CompanyAccountValue::IS_NORMAL_ACCOUNT;
    }
}
