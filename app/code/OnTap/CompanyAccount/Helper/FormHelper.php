<?php
declare(strict_types=1);
namespace OnTap\CompanyAccount\Helper;

use Magento\Framework\Data\Form\FormKey\Validator as FormKeyValidator;
use Magento\Framework\Message\ManagerInterface;

/**
 * Class FormHelper
 *
 * @package OnTap\CompanyAccount\Helper
 */
class FormHelper
{
    /**
     * @var FormKeyValidator
     */
    private $formKeyValidator;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * FormHelper constructor.
     *
     * @param ManagerInterface $messageManager
     * @param FormKeyValidator $formKeyValidator
     */
    public function __construct(
        ManagerInterface $messageManager,
        FormKeyValidator $formKeyValidator
    ) {
        $this->formKeyValidator = $formKeyValidator;
        $this->messageManager = $messageManager;
    }

    /**
     * Validate form key
     *
     * @param \Magento\Framework\App\RequestInterface $request
     * @return bool
     */
    public function validate($request)
    {
        if ($this->formKeyValidator->validate($request)) {
            return true;
        }
        $this->messageManager->addErrorMessage(__('Invalid Form Key. Please refresh the page'));
        return false;
    }
}
