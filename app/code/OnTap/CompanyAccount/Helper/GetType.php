<?php
namespace OnTap\CompanyAccount\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManager;

/**
 * Class GetType
 *
 * @package OnTap\CompanyAccount\Helper
 */
class GetType extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var StoreManager
     */
    private $storeManager;

    /**
     * GetType constructor.
     *
     * @param StoreManager $storeManager
     * @param Context $context
     */
    public function __construct(
        StoreManager $storeManager,
        Context $context
    ) {
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }

    /**
     * Get front end area
     *
     * @return string
     */
    public function getAreaFrontend()
    {
        return \Magento\Framework\App\Area::AREA_FRONTEND;
    }

    /**
     * @return \Magento\Store\Model\StoreManagerInterface
     */
    public function getStoreManager()
    {
        return $this->storeManager;
    }
}
