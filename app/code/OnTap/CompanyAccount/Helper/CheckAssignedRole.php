<?php
declare(strict_types=1);
namespace OnTap\CompanyAccount\Helper;

use OnTap\CompanyAccount\Api\SubUserRepositoryInterface;

/**
 * Class CheckAssignedRole
 *
 * @package OnTap\CompanyAccount\Helper
 */
class CheckAssignedRole
{
    /**
     * @var SubUserRepositoryInterface
     */
    private $subUserRepository;

    /**
     * CheckAssignedRole constructor.
     *
     * @param SubUserRepositoryInterface $subUserRepository
     */
    public function __construct(
        SubUserRepositoryInterface $subUserRepository
    ) {
        $this->subUserRepository = $subUserRepository;
    }

    /**
     * Check if role be assigned to sub-user
     *
     * @param int $roleId
     *
     * @return bool
     */
    public function beAssigned($roleId)
    {
        try {
            if ($this->subUserRepository->getByRole($roleId)->count() > 0) {
                return true;
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }
}
