<?php
declare(strict_types=1);
namespace OnTap\CompanyAccount\Helper;

use OnTap\CompanyAccount\Api\Data\SubRoleInterface as Role;
use OnTap\CompanyAccount\Exception\CantDeleteAssignedRole;
use OnTap\CompanyAccount\Exception\CompareException;
use OnTap\CompanyAccount\Exception\EmptyInputException;

/**
 * Class ActionHelper
 *
 * @package OnTap\CompanyAccount\Helper
 */
class ActionHelper
{
    /**
     * @var CheckAssignedRole
     */
    private $checkAssignedRole;

    /**
     * @var Data
     */
    private $helper;

    /**
     * ActionHelper constructor.
     *
     * @param Data $helper
     * @param CheckAssignedRole $checkAssignedRole
     */
    public function __construct(
        Data $helper,
        CheckAssignedRole $checkAssignedRole
    ) {
        $this->helper = $helper;
        $this->checkAssignedRole = $checkAssignedRole;
    }

    /**
     * Save role
     *
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \OnTap\CompanyAccount\Api\Data\SubRoleInterfaceFactory $roleFactory
     * @param \OnTap\CompanyAccount\Api\SubRoleRepositoryInterface $roleRepository
     * @param int $customerId
     *
     * @return string
     * @throws EmptyInputException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function saveRole($request, $roleFactory, $roleRepository, $customerId)
    {
        $roleId = $request->getParam('role_id', '');
        /** @var \OnTap\CompanyAccount\Api\Data\SubRoleInterface $role */
        $role = $roleFactory->create();
        $role->setRoleName($request->getParam(Role::NAME));
        $roleType = $request->getParam(Role::TYPE);
        if (is_array($roleType)) {
            $roleType = array_filter(
                $roleType,
                function ($value) {
                    return $value != "0";
                }
            );
            $role->setRoleType(implode(',', $roleType));
        } else {
            $role->setRoleType($roleType);
        }
        $maxAmountParam = $request->getParam(Role::MAX_ORDER_AMOUNT);
        $maxAmount = $maxAmountParam ? trim($maxAmountParam) : $maxAmountParam;
        $maxOrderPerDayParam = $request->getParam(Role::MAX_ORDER_AMOUNT);
        $maxOrderPerDay = $maxOrderPerDayParam ? trim($maxOrderPerDayParam) : $maxOrderPerDayParam;
        if ($maxAmount != "") {
            $maxAmount = $this->helper->convertCurrency($maxAmount, false);
        } else {
            $maxAmount = null;
        }
        $role->setMaxOrderPerDay($maxOrderPerDay == "" ? null : $maxOrderPerDay);
        $role->setMaxOrderAmount($maxAmount);
        $role->setCompanyAccount((int) $customerId);

        if (!empty($roleId)) {
            $role->setRoleId((int)$roleId);
            $message = __('Role has been updated.');
        } else {
            $role->setRoleId(null);
            $message = __('New role has been added.');
        }

        $roleRepository->save($role);

        return $message;
    }

    /**
     * Destroy role
     *
     * @param \OnTap\CompanyAccount\Api\SubRoleRepositoryInterface $roleRepository
     * @param int $roleId
     * @param bool $isMassAction
     *
     * @return string
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     * @throws CantDeleteAssignedRole
     */
    public function destroyRole($roleRepository, $roleId, $isMassAction = false)
    {
        if ($this->checkAssignedRole->beAssigned($roleId)) {
            if ($isMassAction) {
                return false;
            }
            throw new CantDeleteAssignedRole(__('You can\'t delete this role, it was assigned to sub-user(s)'));
        }
        if ((int)$roleId !== 0) {
            $roleRepository->deleteById((int)$roleId);
            $message = __('You deleted the role.');
        } else {
            $message = __('We can\'t delete this default admin role for you right now.');
        }
        return $message;
    }

    /**
     * Reset password sub-user
     *
     * @param \OnTap\CompanyAccount\Helper\SubUserHelper $subUserHelper
     * @param \OnTap\CompanyAccount\Helper\EmailHelper $emailHelper
     * @param \Magento\Customer\Api\Data\CustomerInterface|\Magento\Customer\Model\Customer $customer
     * @param int $subId
     *
     * @throws CompareException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function resetPasswordSubUser($subUserHelper, $emailHelper, $customer, $subId)
    {
        /** @var \OnTap\CompanyAccount\Api\Data\SubUserInterface $subUser */
        $subUser = $subUserHelper->getBy($subId);
        if ($subUser->getCompanyCustomerId() != (int) $customer->getId()) {
            throw new CompareException(__('Company Account not match.'));
        }
        $subUser = $subUserHelper->generateResetPasswordToken($subUser);
        $subUserHelper->save($subUser);
        $emailHelper->sendResetPasswordMailToSubUser($customer, $subUser);
    }
}
