define(function () {
    'use strict';

    var ontapCompanyAccountRuleCheck = {
        initConfig: function () {
            this._super();
            if (window.checkoutConfig.cant_create_address) {
                this.addressOptions = this.addressOptions.filter(item => item.customerAddressId !== null);
            }
        }
    };
    return function (target) {
        return target.extend(ontapCompanyAccountRuleCheck);
    };
});
