define([
    'jquery',
    'underscore',
    'mage/translate'
], function ($, _, $t) {
    'use strict';

    $.widget('ontap.company_account_confirmation', {
        options: {
            url: '',
            method: 'post',
            triggerEvent: 'click',
            title: $t('Delete Role'),
            content: $t('Are you sure you want to delete the role?'),
            modalActionArea: '.modal-footer',
            modalNamespace: 'confirmationModal',
            modalClass: '',
            txtCancel: $t('Cancel'),
            txtOK: $t('OK')
        },

        _create: function () {
            this._bind();
        },

        _bind: function () {
            let self = this;
            self.element.on(self.options.triggerEvent, function (event) {
                event.preventDefault();
                self._addModalToGrid();
                self._createButtons($(this));
                self.createModalElement($(this));

            });
        },

        /**
         * Creates buttons pane.
         */
        _createButtons: function () {
            let self = this;
            self.buttons = $("#confirmationModal").find(self.options.modalActionArea);
            // Clean previous buttons
            self.buttons.empty();
            // Add cancel button
            self.buttons.append('<button type="button" data-dismiss="modal" class="btn btn-secondary">' + self.options.txtCancel + '</button>');
            // Add OK Button
            let okButton = $('<button type="button" class="btn btn-primary">' + self.options.txtOK + '</button>');
            self.buttons.append(okButton);
            okButton.on('click', _.bind(function (event) {
                self._ajaxSubmit()
            }, this));
        },

        createModalElement: function () {

            let confirmationModal = $("#confirmationModal");
            confirmationModal.addClass(this.options.modalClass);
            confirmationModal.find("#confirmationModalLabel").text(this.options.title);
            confirmationModal.find(".modal-body").text(this.options.content);
            confirmationModal.modal();
        },

        _ajaxSubmit: function () {
            let self = this,
                formKey = $('input[name="form_key"]').val() || null;
            $.ajax({
                url: self.options.url,
                type: self.options.method,
                dataType: 'json',
                data: {form_key: formKey},
                beforeSend: function () {
                    // @TODO: implement spinner
                    // $('body').trigger('processStart');
                    let confirmationModal = $("#confirmationModal");
                    confirmationModal.modal('hide');
                    confirmationModal.remove();
                }
            }).done(function (res) {
                if (res) {
                    if (res.remove_row) {
                        let $row = self.element.closest('tr');
                        $row.remove();
                    }
                }
            }).always(function () {
                // @TODO: implement spinner
                // $('body').trigger('processStop');
            });
        },
        _addModalToGrid: function () {
            if (!$('#confirmationModal').length) {
                $("body").append('<div class="modal fade" id="confirmationModal" tabindex="-1" role="dialog" aria-labelledby="confirmationModalLabel" aria-hidden="true">' +
                    '    <div class="modal-dialog" role="document">' +
                    '        <div class="modal-content">' +
                    '            <div class="modal-header">' +
                    '                <h5 class="modal-title" id="confirmationModalLabel">' + this.options.title + '</h5>' +
                    '                <button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
                    '                    <span aria-hidden="true">&times;</span>' +
                    '                </button>' +
                    '            </div>' +
                    '            <div class="modal-body"></div>' +
                    '            <div class="modal-footer"></div>' +
                    '        </div>' +
                    '    </div>' +
                    '</div>')
            }
        }
    });
    return $.ontap.company_account_confirmation;
});
