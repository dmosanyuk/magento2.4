define([
    'jquery',
    'uiComponent'
], function ($, Component) {
    'use strict';
    return Component.extend({
        /** @inheritdoc */
        initialize: function () {
            this._super();
            if (this.cantCreateAddress) {
                $.async(`.secondary button.action.add,
                    .box-shipping-address a.action.edit,
                    .box-billing-address .box-actions .action.edit,
                    .box-billing-address a.action.edit,
                    .actions-toolbar .primary .action.add.primary`, function (element) {
                    $(element).remove();
                });
            }
            if (window.checkoutConfig && window.checkoutConfig.cant_create_address) {
                $.async('.new-address-popup', function (element) {
                    $(element).remove();
                });
            }
        }
    });
});
