define([
    'jquery',
    'Magento_Ui/js/modal/alert',
    'mage/translate',
    'OnTap_CompanyAccount/js/multi-wishlist/ontap-wishlist'
], function ($, mAlert, $t) {
    "use strict";

    var compatibleMultiWishList = {
        _showPopup: function ($this) {
            var url = this.options.url_popup,
                that = this;
            $.ajax({
                url: url,
                async: false
            }).done(function (response) {
                if (response.cant_access) {
                    that.hidePopup();
                    $.ontapfancybox.hideLoading();
                    $.ontapfancybox.helpers.overlay.close();
                    mAlert({
                        title: $t('Opps...!'),
                        content: response.error_message
                    });
                } else {
                    return that._super($this);
                }
            }).fail(function () {
                alert('Your account can not get access this action.');
            });
            return false;
        },
    };

    return function (targetWidget) {
        $.widget('mage.MultiWishlist', targetWidget, compatibleMultiWishList); // the widget alias should be like for the target widget

        return $.mage.MultiWishlist; //  the widget by parent alias should be returned
    };
});
