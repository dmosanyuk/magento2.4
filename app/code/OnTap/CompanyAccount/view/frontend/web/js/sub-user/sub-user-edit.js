define([
    'jquery'
], function ($) {
    "use strict";

    $.widget("ontap.subUser", {
        options: {
            beSubmitted: false
        },
        _create: function () {
            this._bind();
        },
        _bind: function () {
            let formData = $('#form-validate');
            $(document).ready(function () {
                formData.submit(function (e) {
                    if (formData.valid()) {
                        $(this).find(':submit').prop('disabled', true);
                    }
                });
                formData.bind("invalid-form.validate", function (e) {
                    $(this).find(':submit').prop('disabled', false);
                });
            });
        }
    });
    return $.ontap.subUser;
});
