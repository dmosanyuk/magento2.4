var config = {
    config: {
        mixins: {
            "OnTap_MultiWishlist/js/ontap_wishlist" : {
                "OnTap_CompanyAccount/js/multi-wishlist/ontap-wishlist": true
            },
            "Magento_Checkout/js/view/billing-address/list": {
                "OnTap_CompanyAccount/js/view/billing-address/list-mixin": true
            }
        }
    },
    map: {
        '*': {
            ontapTreeJs: "OnTap_CompanyAccount/plugins/tree-js/jstree.min"
        }
    }
};
