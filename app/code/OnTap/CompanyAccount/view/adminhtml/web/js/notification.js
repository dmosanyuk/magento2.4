define([
    'jquery',
    'mage/template',
    'OnTap_CompanyAccount/js/notification'
], function ($, mageTemplate) {
    "use strict";

    var ontapSuccessMessage = {
        options: {
            templates: {
                reset_pw_subuser_request_success: '<div data-role="messages" id="messages">' +
                    '<div class="messages"><div class="message message-success success">' +
                    '<div data-ui-id="messages-message-success"><%- data.message %></div></div>' +
                    '</div></div>'
            }
        },

        add: function (data) {
            if (data.reset_pw_subuser_request_success) {
                var template = this.options.templates.reset_pw_subuser_request_success,
                message = mageTemplate(template, {
                    data: data
                }),
                messageContainer;

                if (typeof data.insertMethod === 'function') {
                    data.insertMethod(message);
                } else {
                    messageContainer = data.messageContainer || this.placeholder;
                    $(messageContainer).prepend(message);
                }

                return this;
            }
            return this._super(data);
        }
    };

    return function (targetWidget) {
        $.widget('mage.notification', targetWidget, ontapSuccessMessage); // the widget alias should be like for the target widget

        return $.mage.notification; //  the widget by parent alias should be returned
    };
});
