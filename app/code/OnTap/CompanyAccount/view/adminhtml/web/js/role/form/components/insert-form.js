
define([
    'Magento_Ui/js/form/components/insert-form'
], function (Insert) {
    'use strict';

    return Insert.extend({
        defaults: {
            listens: {
                responseData: 'onResponse'
            },
            modules: {
                roleListing: '${ $.roleListingProvider }',
                roleModal: '${ $.roleModalProvider }'
            }
        },

        /**
         * Close modal, reload customer address listing and save customer address
         *
         * @param {Object} responseData
         */
        onResponse: function (responseData) {
            if (!responseData.error) {
                this.roleModal().closeModal();
                this.roleListing().reload({
                    refresh: true
                });
            }
        },

        /**
         * Event method that closes "Edit role" modal and refreshes grid after role
         * was removed through "Delete" button on the "Edit role" modal
         *
         * @param {String} id - role ID to delete
         */
        onRoleDelete: function (id) {
            this.roleModal().closeModal();
            this.roleListing().reload({
                refresh: true
            });
        }
    });
});
