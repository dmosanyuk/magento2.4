
define([
    'jquery',
    'Magento_Ui/js/form/components/insert-form',
    'OnTap_CompanyAccount/js/ontap_notification'
], function ($, Insert, ontapNotification) {
    'use strict';

    return Insert.extend({
        defaults: {
            listens: {
                responseData: 'onResponse'
            },
            modules: {
                subUserListing: '${ $.subUserListingProvider }',
                subUserModal: '${ $.subUserModalProvider }'
            }
        },

        /**
         * Close modal, reload sub-user listing
         *
         * @param {Object} responseData
         */
        onResponse: function (responseData) {
            if (!responseData.error) {
                this.subUserModal().closeModal();
                this.subUserListing().reload({
                    refresh: true
                });
                ontapNotification.ontapNotification(responseData);
            }
        },

        /**
         * Event method that closes "Edit" modal and refreshes grid after sub-user
         * was removed through "Delete" button on the "Edit" modal
         */
        onSubUserDelete: function (responseData) {
            this.subUserModal().closeModal();
            this.subUserListing().reload({
                refresh: true
            });
            ontapNotification.ontapNotification(responseData);
        }
    });
});
