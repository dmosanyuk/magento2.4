<?php

namespace OnTap\CompanyAccount\Model\Api;

use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\NotFoundException;
use OnTap\CompanyAccount\Api\SubUserRepositoryInterface;
use OnTap\CompanyAccount\Exception\EmailValidateException;
use OnTap\CompanyAccount\Helper\EmailHelper;
use OnTap\CompanyAccount\Helper\SubUserHelper;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use OnTap\CompanyAccount\Api\Data\SubUserInterfaceFactory as SubUserFactory;
use OnTap\CompanyAccount\Api\SubUserApiInterface;
use Magento\Framework\Exception\CouldNotSaveException;

/**
 * Class RestSubUser
 * @package OnTap\CompanyAccount\Model\Rest
 */
class RestSubUser implements SubUserApiInterface
{
    /**
     * @var EmailHelper
     */
    protected $emailHelper;

    /**
     * @var SubUserRepositoryInterface
     */
    protected $subUserRepository;

    /**
     * @var SubUserFactory
     */
    protected  $subUserFactory;

    /**
     * @var SubUserHelper
     */
    private $subUserHelper;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * RestSubUser constructor.
     *
     * @param EmailHelper $emailHelper
     * @param SubUserRepositoryInterface $subUserRepository
     * @param SubUserFactory $subUserFactory
     * @param SubUserHelper $subUserHelper
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(
        EmailHelper $emailHelper,
        SubUserRepositoryInterface $subUserRepository,
        SubUserFactory $subUserFactory,
        SubUserHelper $subUserHelper,
        CustomerRepositoryInterface $customerRepository
    ) {
        $this->emailHelper = $emailHelper;
        $this->subUserRepository = $subUserRepository;
        $this->subUserFactory = $subUserFactory;
        $this->subUserHelper = $subUserHelper;
        $this->customerRepository = $customerRepository;
    }

    /**
     * Create sub-user via REST API request
     *
     * @param \OnTap\CompanyAccount\Api\Data\SubUserInterface $subUser
     * @param string $companyAccountEmail
     * @return \Magento\Framework\Phrase|string
     * @throws AlreadyExistsException
     * @throws EmailValidateException
     * @throws LocalizedException
     * @throws NotFoundException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function createSubUserApi(\OnTap\CompanyAccount\Api\Data\SubUserInterface $subUser, string $companyAccountEmail)
    {
        /** @var \Magento\Customer\Model\Customer $customer */
        $companyAccount = $this->customerRepository->get($companyAccountEmail);
        if ($companyAccount->getId() !== null) {
            $isNew = false;
            $isSendMail = true;
            /** @var \OnTap\CompanyAccount\Model\SubUser $user */
            $user = $this->subUserFactory->create();
            $user->setSubUserName($subUser->getSubUserName());
            $user->setRoleId($subUser->getRoleId());
            $user->setSubUserStatus($subUser->getSubUserStatus());
            $user->setCompanyCustomerId($companyAccount->getId());

            $subId = $subUser->getSubUserId();
            if (!empty($subId) || $subId !== 0) {
                try {
                    $currentSubUser = $this->subUserRepository->getById($subId);
                } catch (\Exception $e) {
                    throw new NotFoundException(__("Can not find this sub-user."));
                }
                if ($currentSubUser) {
                    $isSendMail = $currentSubUser->isSentMail();
                    $user->setSubUserEmail($currentSubUser->getSubUserEmail());
                }
                $user->setSubUserId($subId);
                $message = __('Sub-user has been updated.');
            } else {
                $user->setSubUserId(null);
                $subUserEmail = $subUser->getSubUserEmail();
                if (!filter_var($subUserEmail, FILTER_VALIDATE_EMAIL)) {
                    throw new EmailValidateException(__("Please input non-unicode character for email."));
                }
                $user->setSubUserEmail($subUserEmail);
                $message = __('New sub-user has been added.');
                $isNew = true;
            }
            if ($isNew || !$isSendMail) {
                if ($user->getSubUserStatus()) {
                    $user->setIsSentMail(1);
                    $user = $this->subUserHelper->generateResetPasswordToken($user);
                }
            }
            $this->save($user);
            if ($isNew || !$isSendMail) {
                if ($user->getSubUserStatus()) {
                    $this->emailHelper->sendWelcomeMailToSubUser($companyAccount, $user);
                }
            }
        } else {
            $message = __('There is no customer company account with this email.');
        }

        return $message;
    }

    /**
     * Save sub-user
     *
     * @param \OnTap\CompanyAccount\Api\Data\SubUserInterface $subUser
     *
     * @return \OnTap\CompanyAccount\Api\Data\SubUserInterface
     * @throws AlreadyExistsException
     * @throws LocalizedException
     */
    public function save($subUser)
    {
        try {
            return $this->subUserRepository->save($subUser);
        } catch (AlreadyExistsException $e) {
            throw new AlreadyExistsException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        }
    }
}
