<?php
namespace OnTap\CompanyAccount\Model;

use OnTap\CompanyAccount\Helper\Data;
use OnTap\CompanyAccount\Helper\PermissionsChecker;
use OnTap\CompanyAccount\Model\Config\Source\Permissions;

/**
 * Class CompositeConfigProvider
 *
 * @package OnTap\CompanyAccount\Model
 */
class CompositeConfigProvider implements \Magento\Checkout\Model\ConfigProviderInterface
{
    /**
     * @var Data
     */
    private $helper;

    /**
     * @var PermissionsChecker
     */
    private $checker;

    /**
     * CompositeConfigProvider constructor.
     *
     * @param Data $helper
     * @param PermissionsChecker $checker
     */
    public function __construct(
        Data $helper,
        PermissionsChecker $checker
    ) {
        $this->helper = $helper;
        $this->checker = $checker;
    }

    /**
     * @inheritDoc
     */
    public function getConfig()
    {
        $output = [];
        try {
            if ($this->helper->isEnable() &&
                $this->helper->getCustomerSession()->getSubUser()
            ) {
                $output['cant_create_address'] = $this->checker->isDenied(Permissions::ADD_VIEW_ADDRESS_BOOK);
            }
            return $output;
        } catch (\Exception $e) {
            return $output;
        }
    }
}
