<?php
namespace OnTap\CompanyAccount\Model\ResourceModel\SubUser;

/**
 * Class Collection
 *
 * @package OnTap\CompanyAccount\Model\ResourceModel\SubUser
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'sub_id';

    /**
     * Define resource model.
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(
            \OnTap\CompanyAccount\Model\SubUser::class,
            \OnTap\CompanyAccount\Model\ResourceModel\SubUser::class
        );
    }

    /**
     * Load role detail for sub-user
     *
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection|void
     */
    protected function _initSelect()
    {
        $this->addFilterToMap('customer_id', 'main_table.customer_id');
        $this->addFilterToMap('role_id', 'main_table.role_id');
        $this->addFilterToMap('role_name', 'detail_role.role_name');
        parent::_initSelect();
        $this->getSelect()->joinLeft(
            ['subuser_role' => $this->getTable('ontap_sub_role')],
            'main_table.role_id = subuser_role.role_id',
            []
        )->joinLeft(
            ['detail_role' => $this->getTable('ontap_sub_role')],
            'subuser_role.role_id = detail_role.role_id',
            ['role_name']
        );
    }
}
