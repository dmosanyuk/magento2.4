<?php
namespace OnTap\CompanyAccount\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class SubUser
 *
 * @package OnTap\CompanyAccount\Model\ResourceModel
 */
class SubUserOrder extends AbstractDb
{
    const TABLE = 'ontap_sub_user_order';
    const ID = 'entity_id';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(self::TABLE, self::ID);
    }

    /**
     * Load data by order id
     *
     * @param int $orderId
     * @param \OnTap\CompanyAccount\Model\SubUserOrder $subUserOrder
     * @return SubUserOrder
     */
    public function loadByOrderId($orderId, $subUserOrder)
    {
        $connection = $this->getConnection();
        $select = $this->_getLoadSelect('order_id', $orderId, $subUserOrder);
        $data = $connection->fetchRow($select);

        if ($data) {
            $subUserOrder->setData($data);
        }

        $this->_afterLoad($subUserOrder);

        return $this;
    }
}
