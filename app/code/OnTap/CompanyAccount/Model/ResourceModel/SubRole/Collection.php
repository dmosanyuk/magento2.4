<?php
namespace OnTap\CompanyAccount\Model\ResourceModel\SubRole;

/**
 * Class Collection
 *
 * @package OnTap\CompanyAccount\Model\ResourceModel\SubRole
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /** @var string */
    protected $_idFieldName = 'role_id';

    /**
     * Define resource model.
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(
            \OnTap\CompanyAccount\Model\SubRole::class,
            \OnTap\CompanyAccount\Model\ResourceModel\SubRole::class
        );
    }
}
