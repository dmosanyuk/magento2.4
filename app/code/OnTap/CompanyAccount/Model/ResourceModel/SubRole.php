<?php
namespace OnTap\CompanyAccount\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class SubRole
 *
 * @package OnTap\CompanyAccount\Model\ResourceModel
 */
class SubRole extends AbstractDb
{
    const TABLE_NAME = 'ontap_sub_role';
    const ID = 'role_id';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, self::ID);
    }
}
