<?php
namespace OnTap\CompanyAccount\Model\ResourceModel\SubUserOrder;

/**
 * Class Collection
 *
 * @package OnTap\CompanyAccount\Model\ResourceModel\SubUserOrder
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'entity_id';

    /**
     * Define resource model.
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(
            \OnTap\CompanyAccount\Model\SubUserOrder::class,
            \OnTap\CompanyAccount\Model\ResourceModel\SubUserOrder::class
        );
    }
}
