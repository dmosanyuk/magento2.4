<?php
namespace OnTap\CompanyAccount\Model\Config\Source;

/**
 * Class CompanyAccountValue
 *
 * @package OnTap\CompanyAccount\Model\Config\Source
 */
class CompanyAccountValue
{
    const IS_COMPANY_ACCOUNT = 1;
    const IS_NORMAL_ACCOUNT = 0;
}
