<?php
namespace OnTap\CompanyAccount\Model\Config\Source\SubRole;

/**
 * Class Reader
 *
 * Loads catalog attributes configuration from multiple XML files by merging them together
 *
 * @package OnTap\CompanyAccount\Model\Config\Source\SubRole
 */
class Reader extends \Magento\Framework\Config\Reader\Filesystem
{
    /**
     * List of identifier attributes for merging
     *
     * @var array
     */
    protected $_idAttributes = ['/config/acl/rules(/rule)+' => 'value'];
}
