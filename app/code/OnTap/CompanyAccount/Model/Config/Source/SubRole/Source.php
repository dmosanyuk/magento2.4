<?php
namespace OnTap\CompanyAccount\Model\Config\Source\SubRole;

/**
 * Class Config
 */
class Source
{
    /**
     * @var \Magento\Framework\Config\DataInterface
     */
    protected $dataStorage;

    /**
     * @param \Magento\Framework\Config\DataInterface $dataStorage
     */
    public function __construct(\Magento\Framework\Config\DataInterface $dataStorage)
    {
        $this->dataStorage = $dataStorage;
    }

    /**
     * Get data of rules
     *
     * @return array
     */
    public function getRuleOptionArray()
    {
        return $this->dataStorage->get('rules');
    }
}
