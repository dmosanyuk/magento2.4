<?php
namespace OnTap\CompanyAccount\Model\Config\Source;

use OnTap\CompanyAccount\Api\Data\SubRoleInterface as Role;
use OnTap\CompanyAccount\Api\SubUserRepositoryInterface;
use OnTap\CompanyAccount\Model\ResourceModel\SubRole\Grid\CollectionFactory;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class EnableDisable
 *
 * @package OnTap\CompanyAccount\Model\Config\Source
 */
class RoleOptions implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * @var AbstractCollection
     */
    private $collection;

    /**
     * @var SubUserRepositoryInterface
     */
    private $subUserRepository;

    /**
     * Role constructor.
     *
     * @param \Magento\Framework\App\RequestInterface $request
     * @param SubUserRepositoryInterface $subUserRepository
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        SubUserRepositoryInterface $subUserRepository,
        CollectionFactory $collectionFactory
    ) {
        $this->request = $request;
        $this->collection = $collectionFactory->create();
        $this->subUserRepository = $subUserRepository;
    }

    /**
     * Get list role by customer
     *
     * @return array
     */
    public function getListRole()
    {
        $collection = $this->getCollection();
        $customerId = $this->request->getParam("customer_id");
        if (!$customerId) {
            $customerId = $this->subUserRepository->getById(
                $this->request->getParam('sub_id')
            )->getCompanyCustomerId();
        }
        if ($customerId) {
            $collection->addFieldToFilter(
                [
                    Role::CUSTOMER_ID,
                    Role::CUSTOMER_ID
                ],
                [
                    ["eq" => (int) $customerId],
                    ["null" => true]
                ]
            );
        }

        return $collection->getdata();
    }

    /**
     * Get role collection
     *
     * @return \OnTap\CompanyAccount\Model\ResourceModel\SubRole\Grid\Collection|AbstractCollection
     */
    private function getCollection()
    {
        return $this->collection;
    }

    /**
     * Get enable/disable option
     *
     * @return array
     */
    public function toOptionArray()
    {
        $roles = $this->getListRole();
        $data = [];
        foreach ($roles as $role) {
            $data[] = [
                'label' => $role[Role::NAME],
                'value' => $role[Role::ID]
            ];
        }
        return $data;
    }
}
