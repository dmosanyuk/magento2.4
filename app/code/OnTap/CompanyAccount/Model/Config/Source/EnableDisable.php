<?php
namespace OnTap\CompanyAccount\Model\Config\Source;

/**
 * Class EnableDisable
 *
 * @package OnTap\CompanyAccount\Model\Config\Source
 */
class EnableDisable implements \Magento\Framework\Option\ArrayInterface
{
    const ENABLE = 1;
    const DISABLE = 0;

    /**
     * Get enable/disable option
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'label' => __('Enable'),
                'value' => self::ENABLE
            ],
            [
                'label' => __('Disable'),
                'value' => self::DISABLE
            ]
        ];
    }
}
