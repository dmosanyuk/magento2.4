<?php
namespace OnTap\CompanyAccount\Model;

use OnTap\CompanyAccount\Api\Data\SubRoleInterface;
use OnTap\CompanyAccount\Api\SubRoleRepositoryInterface;
use OnTap\CompanyAccount\Model\ResourceModel\SubRole as RoleResource;
use Magento\Framework\Api\SearchCriteria\CollectionProcessor;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class SubRoleRepository
 *
 * @package OnTap\CompanyAccount\Model
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class SubRoleRepository implements SubRoleRepositoryInterface
{
    /**
     * @var RoleResource
     */
    private $roleResource;

    /**
     * @var SubRoleFactory
     */
    private $roleFactory;

    /**
     * @var RoleResource\CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var SearchResultsInterfaceFactory
     */
    private $searchResultsFactory;

    /**
     * @var CollectionProcessor
     */
    private $collectionProcessor;

    /**
     * SubRoleRepository constructor.
     *
     * @param RoleResource $roleResource
     * @param SubRoleFactory $roleFactory
     * @param CollectionProcessor $collectionProcessor
     * @param RoleResource\CollectionFactory $collectionFactory
     * @param SearchResultsInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        RoleResource $roleResource,
        SubRoleFactory $roleFactory,
        CollectionProcessor $collectionProcessor,
        ResourceModel\SubRole\CollectionFactory $collectionFactory,
        SearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->roleResource = $roleResource;
        $this->roleFactory = $roleFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * Get role by id
     *
     * @param int $id
     * @return SubRoleInterface
     * @throws NoSuchEntityException
     */
    public function getById($id)
    {
        try {
            $role = $this->roleFactory->create();
            $this->roleResource->load($role, $id);

            return $role;
        } catch (\Exception $e) {
            throw new NoSuchEntityException(__('Can not get this role.'));
        }
    }

    /**
     * Save a role
     *
     * @param SubRoleInterface $role
     * @return SubRoleInterface|RoleResource
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function save(SubRoleInterface $role)
    {
        $role = $this->roleResource->save($role);
        return $role;
    }

    /**
     * Retrieve roles matching the specified criteria
     *
     * @param SearchCriteriaInterface $criteria
     * @return \OnTap\CompanyAccount\Api\Data\SubRoleSearchResultsInterface|\Magento\Framework\Api\SearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $criteria)
    {
        /** @var \OnTap\CompanyAccount\Api\Data\SubUserSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        /** @var \OnTap\CompanyAccount\Model\ResourceModel\SubUser\Collection $collection */
        $collection = $this->collectionFactory->create();
        $this->collectionProcessor->process($criteria, $collection);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * Delete role
     *
     * @param SubRoleInterface $role
     * @return bool|RoleResource
     * @throws CouldNotDeleteException
     */
    public function delete(SubRoleInterface $role)
    {
        try {
            return $this->roleResource->delete($role);
        } catch (\Exception $e) {
            throw new CouldNotDeleteException(__($e->getMessage()));
        }
    }

    /**
     * Delete role by id
     *
     * @param int $id
     *
     * @return bool|RoleResource
     * @throws CouldNotDeleteException
     */
    public function deleteById(int $id)
    {
        try {
            $role = $this->roleFactory->create();
            $this->roleResource->load($role, $id);

            return $this->delete($role);
        } catch (\Exception $e) {
            throw new CouldNotDeleteException(__($e->getMessage()));
        }
    }
}
