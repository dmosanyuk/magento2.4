<?php
namespace OnTap\CompanyAccount\Plugin\Sales\History;

use Magento\Framework\Registry;

/**
 * Class Container
 *
 * @package OnTap\CompanyAccount\Plugin\Sales\History
 */
class Container
{
    /**
     * @var Registry
     */
    private $registry;

    /**
     * Container constructor.
     *
     * @param Registry $registry
     */
    public function __construct(
        Registry $registry
    ) {

        $this->registry = $registry;
    }

    /**
     * Register order
     *
     * @param \Magento\Sales\Block\Order\History\Container $subject
     * @param callable $proceed
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @return mixed
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundSetOrder(
        \Magento\Sales\Block\Order\History\Container $subject,
        callable $proceed,
        \Magento\Sales\Api\Data\OrderInterface $order
    ) {
        $this->registry->unregister('ontap_reg_history_order');
        $this->registry->register('ontap_reg_history_order', $order);
        return $proceed($order);
    }
}
