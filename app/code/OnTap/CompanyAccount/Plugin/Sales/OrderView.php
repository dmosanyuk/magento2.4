<?php
namespace OnTap\CompanyAccount\Plugin\Sales;

use OnTap\CompanyAccount\Api\SubRoleRepositoryInterface;
use OnTap\CompanyAccount\Api\SubUserOrderRepositoryInterface;
use OnTap\CompanyAccount\Helper\Data;
use OnTap\CompanyAccount\Model\Config\Source\Permissions;
use Magento\Framework\Controller\Result\RedirectFactory;

/**
 * Class OrderView
 *
 * @package OnTap\CompanyAccount\Plugin\Sales
 */
class OrderView
{
    /**
     * @var Data
     */
    private $helper;

    /**
     * @var SubRoleRepositoryInterface
     */
    private $roleRepository;

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * @var RedirectFactory
     */
    private $redirectFactory;

    /**
     * @var SubUserOrderRepositoryInterface
     */
    private $subUserOrderRepository;

    /**
     * OrderView constructor.
     *
     * @param Data $helper
     * @param SubUserOrderRepositoryInterface $subUserOrderRepository
     * @param RedirectFactory $redirectFactory
     * @param SubRoleRepositoryInterface $roleRepository
     */
    public function __construct(
        Data $helper,
        SubUserOrderRepositoryInterface $subUserOrderRepository,
        RedirectFactory $redirectFactory,
        SubRoleRepositoryInterface $roleRepository
    ) {
        $this->helper = $helper;
        $this->customerSession = $this->helper->getCustomerSession();
        $this->roleRepository = $roleRepository;
        $this->redirectFactory = $redirectFactory;
        $this->subUserOrderRepository = $subUserOrderRepository;
    }

    /**
     * Redirect to home page if sub-user access to no permission page
     *
     * @param \Magento\Sales\Controller\AbstractController\View $subject
     * @param callable $proceed
     * @return bool|\Magento\Framework\Controller\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundExecute(\Magento\Sales\Controller\AbstractController\View $subject, callable $proceed)
    {
        $subUser = $this->customerSession->getSubUser();
        if ($this->helper->isEnable() && $subUser) {
            $relatedRole = $this->roleRepository->getById($subUser->getRelatedRoleId());
            $orders = $this->subUserOrderRepository->getBySubUser($subUser->getSubUserId());
            if ($subUser->canAccess(Permissions::ADMIN, $relatedRole) ||
                $subUser->canAccess(Permissions::VIEW_ALL_ORDER, $relatedRole) ||
                in_array($subject->getRequest()->getParam('order_id'), $orders)
            ) {
                return $proceed();
            }
            $this->helper->getMessageManager()->addErrorMessage(__('You have no permission to access that page.'));
            return $this->redirectFactory->create()
                ->setPath('');
        }
        return $proceed();
    }
}
