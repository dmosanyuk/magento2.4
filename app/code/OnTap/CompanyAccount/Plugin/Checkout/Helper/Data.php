<?php
namespace OnTap\CompanyAccount\Plugin\Checkout\Helper;

use OnTap\CompanyAccount\Helper\PermissionsChecker;
use OnTap\CompanyAccount\Model\Config\Source\Permissions;

/**
 * Class Data
 *
 * @package OnTap\CompanyAccount\Plugin\Checkout\Helper
 */
class Data
{
    /**
     * @var \OnTap\CompanyAccount\Helper\Data
     */
    private $helper;

    /**
     * @var PermissionsChecker
     */
    private $permissionsChecker;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    private $cart;

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * Data constructor.
     *
     * @param \OnTap\CompanyAccount\Helper\Data $helper
     * @param PermissionsChecker $permissionsChecker
     * @param \Magento\Checkout\Model\Cart $cart
     */
    public function __construct(
        \OnTap\CompanyAccount\Helper\Data $helper,
        PermissionsChecker $permissionsChecker,
        \Magento\Checkout\Model\Cart $cart
    ) {
        $this->helper = $helper;
        $this->customerSession = $this->helper->getCustomerSession();
        $this->permissionsChecker = $permissionsChecker;
        $this->cart = $cart;
    }

    /**
     * Disable onepage checkout if sub-user max order amount is invalid
     *
     * @param \Magento\Checkout\Helper\Data $subject
     * @param bool $result
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterCanOnepageCheckout(
        \Magento\Checkout\Helper\Data $subject,
        $result
    ) {
        if ($this->helper->isEnable() && $this->customerSession->getSubUser()) {
            $orderAmount = $this->cart->getQuote()->getBaseSubtotal();
            $cantAccessWithOrderAmount = $this->permissionsChecker
                ->isDenied(Permissions::MAX_ORDER_AMOUNT, $orderAmount);
            $cantAccessWithOrderPerDay = $this->permissionsChecker
                ->isDenied(Permissions::MAX_ORDER_PERDAY);
            if ($cantAccessWithOrderAmount['is_denied'] || $cantAccessWithOrderPerDay['is_denied']) {
                return false;
            }
        }
        return $result;
    }
}
