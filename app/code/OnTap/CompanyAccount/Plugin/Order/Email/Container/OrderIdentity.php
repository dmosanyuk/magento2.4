<?php
namespace OnTap\CompanyAccount\Plugin\Order\Email\Container;

use OnTap\CompanyAccount\Helper\Data;

/**
 * Class OrderIdentity
 *
 * @package OnTap\CompanyAccount\Plugin\Order\Email\Container
 */
class OrderIdentity
{
    /**
     * @var Data
     */
    private $helper;

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * @var \Magento\Framework\Registry
     */
    private $registry;

    /**
     * OrderIdentity constructor.
     *
     * @param \Magento\Framework\Registry $registry
     * @param Data $helper
     */
    public function __construct(
        \Magento\Framework\Registry $registry,
        Data $helper
    ) {
        $this->helper = $helper;
        $this->customerSession = $this->helper->getCustomerSession();
        $this->registry = $registry;
    }

    /**
     * If take order is sub-user
     *
     * @param Object $subject
     * @param array|bool $result
     * @return mixed
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetEmailCopyTo(
        $subject,
        $result
    ) {
        try {
            if ($this->helper->isEnable()) {
                /** @var \OnTap\CompanyAccount\Api\Data\SubUserInterface $subUser */
                $subUser = $this->customerSession->getSubUser();
                if (!$subUser) {
                    $subUser = $this->registry->registry('ontap_is_send_mail_to_sub_user');
                }
                if ($subUser) {
                    $result[] = $subUser->getSubUserEmail();
                }
            }
            return $result;
        } catch (\Exception $e) {
            return $result;
        }
    }
}
