<?php
namespace OnTap\CompanyAccount\Plugin\Order\Email\Container;

/**
 * Class Container
 *
 * @package OnTap\CompanyAccount\Plugin\Order\Email\Container
 */
class Container
{
    /**
     * @var \Magento\Framework\Registry
     */
    private $registry;

    /**
     * Container constructor.
     *
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        \Magento\Framework\Registry $registry
    ) {
        $this->registry = $registry;
    }

    /**
     * If order was placed by sub-user will return sub-user email
     *
     * @param \Magento\Sales\Model\Order\Email\Container\Container $subject
     * @param string $result
     * @return string
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetCustomerEmail($subject, $result)
    {
        $subUser = $this->getSubUser();
        if ($subUser) {
            return $subUser->getSubUserEmail();
        }
        return $result;
    }

    /**
     * If order was placed by sub-user will return sub-user name
     *
     * @param \Magento\Sales\Model\Order\Email\Container\Container $subject
     * @param string $result
     * @return string
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetCustomerName($subject, $result)
    {
        $subUser = $this->getSubUser();
        if ($subUser) {
            return $subUser->getSubUserName();
        }
        return $result;
    }

    /**
     * Get registered sub-user
     *
     * @return \OnTap\CompanyAccount\Api\Data\SubUserInterface
     */
    protected function getSubUser()
    {
        return $this->registry->registry('ontap_is_send_mail_to_sub_user');
    }
}
