<?php
namespace OnTap\CompanyAccount\Plugin\Customer\Permissions;

use OnTap\CompanyAccount\Helper\PermissionsChecker;
use OnTap\CompanyAccount\Model\Config\Source\Permissions;
use Magento\Catalog\Model\Product;
use Magento\Framework\DataObject;

/**
 * Class WishListAdd
 *
 * @package OnTap\CompanyAccount\Plugin\Customer\Permissions
 */
class WishListAdd
{
    /**
     * @var PermissionsChecker
     */
    private $permissionsChecker;

    /**
     * WishListAdd constructor.
     *
     * @param PermissionsChecker $permissionsChecker
     */
    public function __construct(PermissionsChecker $permissionsChecker)
    {
        $this->permissionsChecker = $permissionsChecker;
    }

    /**
     * Check if logged in is sub-user and have permission to access this action
     *
     * @param \Magento\Wishlist\Model\Wishlist $subject
     * @param callable $proceed
     * @param int|Product $product
     * @param DataObject|array|string|null $buyRequest
     * @param bool $forciblySetQty
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundAddNewItem(
        \Magento\Wishlist\Model\Wishlist $subject,
        callable $proceed,
        $product,
        $buyRequest = null,
        $forciblySetQty = false
    ) {
        $checkValue = $this->permissionsChecker->check(Permissions::ADD_VIEW_ACCOUNT_WISHLIST);
        if ($checkValue) {
            $this->permissionsChecker->getMessageManager()->getMessages(true);
            return __('You have no permission to this action.')->__toString();
        }
        return $proceed($product, $buyRequest, $forciblySetQty);
    }
}
