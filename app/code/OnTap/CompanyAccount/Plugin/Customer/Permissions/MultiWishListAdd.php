<?php
namespace OnTap\CompanyAccount\Plugin\Customer\Permissions;

use OnTap\CompanyAccount\Helper\PermissionsChecker;
use OnTap\CompanyAccount\Model\Config\Source\Permissions;
use Magento\Framework\Controller\Result\JsonFactory;

/**
 * Class MultiWishListAdd
 *
 * @package OnTap\CompanyAccount\Plugin\Customer\Permissions
 */
class MultiWishListAdd
{
    /**
     * @var PermissionsChecker
     */
    private $permissionsChecker;

    /**
     * @var JsonFactory
     */
    private $jsonFactory;

    /**
     * MultiWishListAdd constructor.
     *
     * @param PermissionsChecker $permissionsChecker
     * @param JsonFactory $jsonFactory
     */
    public function __construct(
        PermissionsChecker $permissionsChecker,
        JsonFactory $jsonFactory
    ) {
        $this->permissionsChecker = $permissionsChecker;
        $this->jsonFactory = $jsonFactory;
    }

    /**
     * Check if logged in is sub-user and have permission to access this action
     *
     * @param \OnTap\MultiWishlist\Controller\Index\AssignWishlist $subject
     * @param callable $proceed
     * @return \Magento\Framework\Controller\Result\Json
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundExecute(\OnTap\MultiWishlist\Controller\Index\AssignWishlist $subject, callable $proceed)
    {
        $checkValue = $this->permissionsChecker->check(Permissions::ADD_VIEW_ACCOUNT_WISHLIST);
        if ($checkValue) {
            $this->permissionsChecker->getMessageManager()->getMessages(true);
            $this->permissionsChecker->getMessageManager()->addErrorMessage(
                __('You have no permission to this action.')
            );
            return $this->jsonFactory->create()->setData([]);
        }
        return $proceed();
    }
}
