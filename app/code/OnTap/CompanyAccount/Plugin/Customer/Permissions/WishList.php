<?php
namespace OnTap\CompanyAccount\Plugin\Customer\Permissions;

use OnTap\CompanyAccount\Helper\PermissionsChecker;
use OnTap\CompanyAccount\Model\Config\Source\Permissions;

/**
 * Class WishList
 *
 * @package OnTap\CompanyAccount\Plugin\Customer\Permissions
 */
class WishList
{
    /**
     * @var PermissionsChecker
     */
    private $permissionsChecker;

    /**
     * WishList constructor.
     *
     * @param PermissionsChecker $permissionsChecker
     */
    public function __construct(PermissionsChecker $permissionsChecker)
    {
        $this->permissionsChecker = $permissionsChecker;
    }

    /**
     * Check if logged in is sub-user and have permission to access this action
     *
     * @param \Magento\Wishlist\Controller\Index\Index $subject
     * @param callable $proceed
     * @return \Magento\Framework\Controller\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundExecute(\Magento\Wishlist\Controller\Index\Index $subject, callable $proceed)
    {
        $checkValue = $this->permissionsChecker->check(Permissions::ADD_VIEW_ACCOUNT_WISHLIST);
        if ($checkValue) {
            return $checkValue;
        }
        return $proceed();
    }
}
