<?php
namespace OnTap\CompanyAccount\Plugin\Customer\Permissions;

use OnTap\CompanyAccount\Helper\PermissionsChecker;
use OnTap\CompanyAccount\Model\Config\Source\Permissions;

/**
 * Class DownloadableProducts
 *
 * @package OnTap\CompanyAccount\Plugin\Customer\Permissions
 */
class DownloadableProducts
{
    /**
     * @var PermissionsChecker
     */
    private $permissionsChecker;

    /**
     * DownloadableProducts constructor.
     *
     * @param PermissionsChecker $permissionsChecker
     */
    public function __construct(PermissionsChecker $permissionsChecker)
    {
        $this->permissionsChecker = $permissionsChecker;
    }

    /**
     * Check if logged in is sub-user and have permission to access this action
     *
     * @param \Magento\Downloadable\Controller\Customer\Products $subject
     * @param callable $proceed
     * @return \Magento\Framework\Controller\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundExecute(\Magento\Downloadable\Controller\Customer\Products $subject, callable $proceed)
    {
        $checkValue = $this->permissionsChecker->check(Permissions::VIEW_DOWNLOADABLE_PRODUCT);
        if ($checkValue) {
            return $checkValue;
        }
        return $proceed();
    }
}
