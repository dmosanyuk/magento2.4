<?php
namespace OnTap\CompanyAccount\Plugin\Customer\Permissions;

use OnTap\CompanyAccount\Helper\PermissionsChecker;
use OnTap\CompanyAccount\Model\Config\Source\Permissions;

/**
 * Class MultiWishListAddFromCart
 *
 * @package OnTap\CompanyAccount\Plugin\Customer\Permissions
 */
class MultiWishListAddFromCart
{
    /**
     * @var PermissionsChecker
     */
    private $permissionsChecker;

    /**
     * MultiWishListAddFromCart constructor.
     *
     * @param PermissionsChecker $permissionsChecker
     */
    public function __construct(
        PermissionsChecker $permissionsChecker
    ) {
        $this->permissionsChecker = $permissionsChecker;
    }

    /**
     * Check if logged in is sub-user and have permission to access this action
     *
     * @param \OnTap\MultiWishlist\Controller\Index\AssignWishlistFromCart $subject
     * @param callable $proceed
     * @return \Magento\Framework\Controller\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundExecute(
        \OnTap\MultiWishlist\Controller\Index\AssignWishlistFromCart $subject,
        callable $proceed
    ) {
        $checkValue = $this->permissionsChecker->check(Permissions::ADD_VIEW_ACCOUNT_WISHLIST);
        if ($checkValue) {
            $this->permissionsChecker->getMessageManager()->getMessages(true);
            $this->permissionsChecker->getMessageManager()->addErrorMessage(
                __('You have no permission to this action.')
            );
            return $checkValue;
        }
        return $proceed();
    }
}
