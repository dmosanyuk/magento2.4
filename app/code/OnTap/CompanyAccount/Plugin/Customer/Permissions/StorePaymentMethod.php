<?php
namespace OnTap\CompanyAccount\Plugin\Customer\Permissions;

use OnTap\CompanyAccount\Helper\PermissionsChecker;
use OnTap\CompanyAccount\Model\Config\Source\Permissions;

/**
 * Class StorePaymentMethod
 *
 * @package OnTap\CompanyAccount\Plugin\Customer\Permissions
 */
class StorePaymentMethod
{
    /**
     * @var PermissionsChecker
     */
    private $permissionsChecker;

    /**
     * StorePaymentMethod constructor.
     *
     * @param PermissionsChecker $permissionsChecker
     */
    public function __construct(PermissionsChecker $permissionsChecker)
    {
        $this->permissionsChecker = $permissionsChecker;
    }

    /**
     * Check if logged in is sub-user and have permission to access this action
     *
     * @param \Magento\Vault\Controller\Cards\ListAction $subject
     * @param callable $proceed
     * @return \Magento\Framework\Controller\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundExecute(\Magento\Vault\Controller\Cards\ListAction $subject, callable $proceed)
    {
        $checkValue = $this->permissionsChecker->check(Permissions::VIEW_STORED_PAYMENT_METHOD);
        if ($checkValue) {
            return $checkValue;
        }
        return $proceed();
    }
}
