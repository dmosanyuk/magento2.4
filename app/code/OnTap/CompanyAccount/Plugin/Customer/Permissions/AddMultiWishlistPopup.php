<?php
namespace OnTap\CompanyAccount\Plugin\Customer\Permissions;

use OnTap\CompanyAccount\Helper\PermissionsChecker;
use OnTap\CompanyAccount\Model\Config\Source\Permissions;
use Magento\Framework\Controller\Result\JsonFactory;

/**
 * Class AddMultiWishlistPopup
 *
 * @package OnTap\CompanyAccount\Plugin\Customer\Permissions
 */
class AddMultiWishlistPopup
{
    /**
     * @var PermissionsChecker
     */
    private $permissionsChecker;

    /**
     * @var JsonFactory
     */
    private $jsonFactory;

    /**
     * MultiWishListAddFromCart constructor.
     *
     * @param PermissionsChecker $permissionsChecker
     * @param JsonFactory $jsonFactory
     */
    public function __construct(
        PermissionsChecker $permissionsChecker,
        JsonFactory $jsonFactory
    ) {
        $this->permissionsChecker = $permissionsChecker;
        $this->jsonFactory = $jsonFactory;
    }

    /**
     * Check if logged in is sub-user and have permission to access this action
     *
     * @param \OnTap\MultiWishlist\Controller\Index\Popup $subject
     * @param callable $proceed
     * @return \Magento\Framework\Controller\Result\Json
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundExecute(
        \OnTap\MultiWishlist\Controller\Index\Popup $subject,
        callable $proceed
    ) {
        $checkValue = $this->permissionsChecker->check(Permissions::ADD_VIEW_ACCOUNT_WISHLIST);
        if ($checkValue) {
            $result = $this->jsonFactory->create();
            $this->permissionsChecker->getMessageManager()->getMessages(true);
            $result->setData(
                [
                'cant_access' => true,
                'error_message' => __('You have no permission to this action.')
                ]
            );
            return $result;
        }
        return $proceed();
    }
}
