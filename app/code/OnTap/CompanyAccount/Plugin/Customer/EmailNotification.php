<?php
namespace OnTap\CompanyAccount\Plugin\Customer;

/**
 * Class EmailNotification
 *
 * @package OnTap\CompanyAccount\Plugin\customer
 */
class EmailNotification
{
    /**
     * @var \Magento\Framework\Registry
     */
    private $registry;

    /**
     * EmailNotification constructor.
     *
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        \Magento\Framework\Registry $registry
    ) {
        $this->registry = $registry;
    }

    /**
     * Before send changed mail notification
     *
     * @param \Magento\Customer\Model\EmailNotification $subject
     * @param \Magento\Customer\Api\Data\CustomerInterface $savedCustomer
     * @param string $origCustomerEmail
     * @return array
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeCredentialsChanged(
        \Magento\Customer\Model\EmailNotification $subject,
        $savedCustomer,
        $origCustomerEmail
    ) {
        $alreadyExistsEmail = $this->registry->registry('already_exists_email');
        if ($alreadyExistsEmail) {
            $savedCustomer->setEmail('no_send_mail');
            $origCustomerEmail = 'no_send_mail';
        }

        return [$savedCustomer, $origCustomerEmail];
    }
}
