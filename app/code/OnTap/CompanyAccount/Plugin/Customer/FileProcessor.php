<?php
namespace OnTap\CompanyAccount\Plugin\Customer;

use Psr\Log\LoggerInterface;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\MediaStorage\Model\File\Uploader;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Framework\Filesystem\Directory\WriteInterface;

/**
 * Class FileProcessor
 * @package OnTap\CompanyAccount\Plugin\Customer
 */
class FileProcessor
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Magento\Framework\Filesystem\Directory\WriteInterface
     */
    private $mediaDirectory;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    private $uploaderFactory;

    /**
     * FileProcessor constructor.
     * @param LoggerInterface $logger
     * @param UploaderFactory $uploaderFactory
     * @param Filesystem $filesystem
     * @throws FileSystemException
     */
    public function __construct(
        LoggerInterface $logger,
        UploaderFactory $uploaderFactory,
        Filesystem $filesystem
    ) {
        $this->mediaDirectory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->logger = $logger;
        $this->uploaderFactory = $uploaderFactory;
    }

    /**
     * @param \Magento\Customer\Model\FileProcessor $subject
     * @param callable $proceed
     * @param $fileId
     * @return array|bool
     * @throws LocalizedException
     */
    public function aroundSaveTemporaryFile(\Magento\Customer\Model\FileProcessor $subject, callable $proceed, $fileId)
    {
        /** @var \Magento\MediaStorage\Model\File\Uploader $uploader */
        $uploader = $this->uploaderFactory->create(['fileId' => $fileId]);
        $uploader->setFilesDispersion(false);
        $uploader->setFilenamesCaseSensitivity(false);
        $uploader->setAllowRenameFiles(true);
        $uploader->setAllowedExtensions([]);

        $path = $this->mediaDirectory->getAbsolutePath(
            'customer/tmp'
        );

        $guidFileName = md5(microtime(true)) . '.' . $uploader->getFileExtension();

        $result = $uploader->save($path, $guidFileName);
        unset($result['path']);
        if (!$result) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('File can not be saved to the destination folder.')
            );
        }

        return $result;
    }

    /**
     * Saving company logos to ontap directory
     *
     * @param \Magento\Customer\Model\FileProcessor $subject
     * @param callable $proceed
     * @param $fileName
     * @return string
     * @throws LocalizedException
     */
    public function aroundMoveTemporaryFile(
        \Magento\Customer\Model\FileProcessor $subject,
        callable $proceed,
        $fileName
    ) {
        $proceed($fileName);

        $fileName = ltrim($fileName, '/');

        $dispersionPath = Uploader::getDispersionPath($fileName);
        $destinationPath = 'ontap/customer' . $dispersionPath;
        $sourcePath = 'customer' . $dispersionPath;

        if (!$this->mediaDirectory->create($destinationPath)) {
            throw new LocalizedException(
                __('Unable to create directory %1.', $destinationPath)
            );
        }

        if (!$this->mediaDirectory->isWritable($destinationPath)) {
            throw new LocalizedException(
                __('Destination folder is not writable or does not exists.')
            );
        }

        $destinationFileName = Uploader::getNewFileName(
            $this->mediaDirectory->getAbsolutePath($destinationPath) . '/' . $fileName
        );

        try {
            //@todo: Technical debt. Workaround for mediaDirectory->renameFile, which called twice
            $this->mediaDirectory->copyFile(
                $sourcePath . '/'. $fileName,
                $destinationPath . '/' . $destinationFileName
            );
        } catch (\Exception $e) {
            $this->logger->critical($e);
            throw new LocalizedException(
                __('Something went wrong while saving the file.')
            );
        }

        $fileName = $dispersionPath . '/' . $destinationFileName;
        return $fileName;
    }
}
