<?php
namespace OnTap\CompanyAccount\Plugin\Customer;

use OnTap\CompanyAccount\Helper\Data;
use OnTap\CompanyAccount\Model\ResourceModel\Customer;

/**
 * Class AccountManagement
 *
 * @package OnTap\CompanyAccount\Plugin\Customer\AccountManagement
 */
class AccountManagement
{
    /**
     * @var Customer
     */
    private $customerResource;

    /**
     * @var Data
     */
    private $helper;

    /**
     * AccountManagement constructor.
     *
     * @param Data $helper
     * @param Customer $customerResource
     */
    public function __construct(
        Data $helper,
        Customer $customerResource
    ) {
        $this->customerResource = $customerResource;
        $this->helper = $helper;
    }

    /**
     * Validate with sub-user email
     *
     * @param \Magento\Customer\Model\AccountManagement $subject
     * @param callable $proceed
     * @param string $customerEmail
     * @param null|int $websiteId
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundIsEmailAvailable(
        \Magento\Customer\Model\AccountManagement $subject,
        callable $proceed,
        $customerEmail,
        $websiteId = null
    ) {
        $result = $proceed($customerEmail, $websiteId);
        if ($this->helper->isEnable()) {
            if (!$websiteId) {
                $websiteId = $this->helper->getWebsiteId();
            }
            $customerEmailResult = $this->customerResource->validateUniqueCustomer($customerEmail, $websiteId);
            if ($customerEmailResult) {
                return false;
            }
            return $result;
        }
        return $result;
    }
}
