<?php
namespace OnTap\CompanyAccount\Plugin\Customer;

use Magento\Customer\Model\Session;

/**
 * Class CustomerData
 *
 * @package OnTap\CompanyAccount\Plugin\Customer
 *
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 */
class CustomerData
{
    /**
     * @var Session
     */
    private $customerSession;

    /**
     * CustomerData constructor.
     *
     * @param Session $customerSession
     */
    public function __construct(
        Session $customerSession
    ) {
        $this->customerSession = $customerSession;
    }

    /**
     * After get customer session data
     *
     * Will change full name of customer to sub-user name whenever
     * account login in is sub-user account
     *
     * @param \Magento\Customer\CustomerData\Customer $subject
     * @param array $result
     *
     * @return array
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetSectionData(\Magento\Customer\CustomerData\Customer $subject, $result)
    {
        /** @var \OnTap\CompanyAccount\Api\Data\SubUserInterface $subUser */
        $subUser = $this->customerSession->getSubUser();
        if ($subUser) {
            $result['fullname'] = $subUser->getSubUserName() . " [" . $result['fullname'] . "]";
            $result['subuser_name'] = $subUser->getSubUserName();
            $result['subuser_email'] = $subUser->getSubUserEmail();
        }
        $result['company'] = $this->customerSession->getCustomer()->getCompanyName();

        return $result;
    }
}
