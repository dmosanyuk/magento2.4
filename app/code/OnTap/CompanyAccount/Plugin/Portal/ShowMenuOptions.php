<?php

namespace OnTap\CompanyAccount\Plugin\Portal;

use Magento\Framework\Exception\LocalizedException;
use OnTap\CompanyAccount\Helper\Data;
use OnTap\CompanyAccount\Helper\PermissionsChecker;
use OnTap\CompanyAccount\Model\Config\Source\Permissions;
use Psr\Log\LoggerInterface;

class ShowMenuOptions
{
    const MENU_OPTION_PREFIX = 'OnTap_CompanyAccount';
    /**
     * Portal menu and ACL Relationship
     * [ menu id => Permission constant]
     *
     * @var array
     */
    protected $ACLRelation = [
        'OnTap_CompanyAccount::company_manage_users' => Permissions::MANAGE_SUB_USER_AND_ROLES,
        'OnTap_CompanyAccount::company_manage_roles' => Permissions::MANAGE_SUB_USER_AND_ROLES
    ];
    /**
     * @var Data
     */
    protected $helper;
    /**
     * @var PermissionsChecker
     */
    protected $permissionsChecker;
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var \OnTap\Portal\Model\Menu\Item
     */
    protected $menuItem;

    /**
     * @param Data               $helper
     * @param PermissionsChecker $permissionsChecker
     * @param LoggerInterface    $logger
     */
    public function __construct(
        Data $helper,
        PermissionsChecker $permissionsChecker,
        LoggerInterface $logger
    ) {
        $this->helper             = $helper;
        $this->permissionsChecker = $permissionsChecker;
        $this->logger             = $logger;
    }

    /**
     * Show menu option to if user has a company account
     *
     * @param                                          $result
     * @param \OnTap\Portal\Model\Menu\Filter\Iterator $subject
     *
     * @return bool
     */
    public function afterAccept(\OnTap\Portal\Model\Menu\Filter\Iterator $subject, $result)
    {
        try {
            $this->menuItem = $subject->current();
            if ($result
                && $this->helper->isEnable()
                && $this->isCompanyAccountResource()
            ) {
                return $this->helper->isCompanyAccount() && !$this->isDenied();
            }
        } catch (LocalizedException $e) {
            $this->logger->error($e->getMessage());
        }

        return $result;
    }

    /**
     * Check if menu option is denied
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @return bool
     */
    private function isDenied()
    {
        $aclPermission = $this->ACLRelation[$this->menuItem->getId()] ?? null;
        if ($aclPermission) {
            $result = $this->permissionsChecker->isDenied($aclPermission);
            if (is_array($result)) {
                return $result['is_denied'] ?? false;
            }

            return $result;
        }

        return false;
    }

    /**
     * Check if resource is from the current module
     *
     * @return false|int
     */
    private function isCompanyAccountResource()
    {
        $itemData = $this->menuItem->toArray();
        $menuId = $itemData['id'] ?? '';

        return strpos($menuId, self::MENU_OPTION_PREFIX) === 0;
    }
}
