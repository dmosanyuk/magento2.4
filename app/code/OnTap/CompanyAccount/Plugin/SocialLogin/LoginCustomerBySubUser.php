<?php
/**
 * Copyright © On Tap Networks Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OnTap\CompanyAccount\Plugin\SocialLogin;

class LoginCustomerBySubUser
{
    /**
     * @var \OnTap\CompanyAccount\Api\SubUserManagementInterface
     */
    protected $subUserManagement;
    /**
     * @var \OnTap\CompanyAccount\Registry\SocialLogin
     */
    protected $socialLoginRegistry;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var \Mageplaza\SocialLogin\Model\ResourceModel\Social\CollectionFactory
     */
    protected $socialCollectionFactory;
    /**
     * @var \OnTap\CompanyAccount\Helper\Data
     */
    protected $companyAccountHelper;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @param \OnTap\CompanyAccount\Api\SubUserManagementInterface                $subUserManagement
     * @param \OnTap\CompanyAccount\Registry\SocialLogin                          $socialLoginRegistry
     * @param \Magento\Store\Model\StoreManagerInterface                          $storeManager
     * @param \Mageplaza\SocialLogin\Model\ResourceModel\Social\CollectionFactory $socialCollectionFactory
     * @param \OnTap\CompanyAccount\Helper\Data                                   $companyAccountHelper
     * @param \Psr\Log\LoggerInterface                                            $logger
     */
    public function __construct(
        \OnTap\CompanyAccount\Api\SubUserManagementInterface $subUserManagement,
        \OnTap\CompanyAccount\Registry\SocialLogin $socialLoginRegistry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Mageplaza\SocialLogin\Model\ResourceModel\Social\CollectionFactory $socialCollectionFactory,
        \OnTap\CompanyAccount\Helper\Data $companyAccountHelper,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->subUserManagement       = $subUserManagement;
        $this->socialLoginRegistry     = $socialLoginRegistry;
        $this->storeManager            = $storeManager;
        $this->socialCollectionFactory = $socialCollectionFactory;
        $this->companyAccountHelper    = $companyAccountHelper;
        $this->logger                  = $logger;
    }

    /**
     * Change the identifier to login as the main customer account
     *
     * @see \Mageplaza\SocialLogin\Model\Social::getCustomerBySocial
     *
     * @param callable                            $proceed
     * @param int|string                          $identify
     * @param string                              $type
     * @param \Mageplaza\SocialLogin\Model\Social $subject
     *
     * @return \Magento\Customer\Model\Customer
     */
    public function aroundGetCustomerBySocial(
        \Mageplaza\SocialLogin\Model\Social $subject,
        callable $proceed,
        $identify,
        $type
    ) {
        try {
            $websiteId = $this->storeManager->getWebsite()->getId();
            if ($this->companyAccountHelper->isEnable($websiteId)) {
                $userProfile = $this->socialLoginRegistry->getUserProfile();
                if ($userProfile && $email = $userProfile->emailVerified) {
                    $subUser = $this->subUserManagement->getSubUserBy($email, 'sub_email', $websiteId);
                    if ($subUser) {
                        $customer = $this->subUserManagement->getCustomerBySubUser($subUser, $websiteId);
                        $identify = $this->getCustomerIdentity($customer, $type);
                    }
                }
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->logger->error($e->getMessage(), ['trace' => $e->getTraceAsString()]);
        }

        return $proceed($identify, $type);
    }

    /**
     * Retrieve Identity from Customer account from
     *
     * @param \Magento\Customer\Model\Data\Customer $customer
     * @param string                                $type
     *
     * @return string|int|null
     */
    protected function getCustomerIdentity(\Magento\Customer\Model\Data\Customer $customer, $type)
    {
        $collection = $this->socialCollectionFactory->create();
        /** @var \Mageplaza\SocialLogin\Model\Social $socialUser */
        $socialUser = $collection->addFieldToFilter('customer_id', $customer->getId())
                                 ->addFieldToFilter('type', $type)
                                 ->addFieldToFilter('status', ['null' => 'true'])
                                 ->getFirstItem();

        return $socialUser->getData('social_id');
    }
}
