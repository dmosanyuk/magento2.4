<?php
namespace OnTap\CompanyAccount\Api;

use OnTap\CompanyAccount\Api\Data\SubRoleInterface;
use OnTap\CompanyAccount\Api\Data\SubRoleSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Interface SubRoleRepositoryInterface
 *
 * @package OnTap\CompanyAccount\Api
 */
interface SubRoleRepositoryInterface
{
    /**
     * Save a role
     *
     * @param SubRoleInterface $role
     * @return SubRoleInterface
     */
    public function save(SubRoleInterface $role);

    /**
     * Get role by id
     *
     * @param int $id
     * @return SubRoleInterface
     * @throws NoSuchEntityException
     */
    public function getById($id);

    /**
     * Retrieve roles matching the specified criteria
     *
     * @param SearchCriteriaInterface $criteria
     * @return SubRoleSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $criteria);

    /**
     * Destroy a role
     *
     * @param SubRoleInterface $role
     *
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(SubRoleInterface $role);

    /**
     * Destroy role by id
     *
     * @param int $id
     *
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function deleteById(int $id);
}
