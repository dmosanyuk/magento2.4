<?php

namespace OnTap\CompanyAccount\Api;

/**
 * Interface SubUserApiInterface
 * @package OnTap\CompanyAccount\Api
 */
interface SubUserApiInterface
{
    /**
     * Create Sub-user
     *
     * @param Data\SubUserInterface $subUser
     * @param string $companyAccountEmail
     * @return string
     */
    public function createSubUserApi(
        \OnTap\CompanyAccount\Api\Data\SubUserInterface $subUser,
        string $companyAccountEmail
    );
}
