<?php
namespace OnTap\CompanyAccount\Api\Data;

/**
 * Interface SubUserOrderInterface
 *
 * @package OnTap\CompanyAccount\Api\Data
 */
interface SubUserOrderInterface
{
    /**
     * Constants for keys of data array.
     */
    const ID = 'entity_id';
    const SUB_USER_ID = 'sub_id';
    const ORDER_ID = 'order_id';
    const GRAND_TOTAL = 'grand_total';
    const SUB_USER_INFO = 'sub_user_info';

    /**
     * Get sub-user info
     *
     * @return array
     */
    public function getSubUserInfo();

    /**
     * Set sub-user info
     *
     * @param string $data
     * @return void
     */
    public function setSubUserInfo($data);

    /**
     * Get id
     *
     * @return int
     */
    public function getId();

    /**
     * Set Id
     *
     * @param int $id
     *
     * @return void
     */
    public function setId($id);

    /**
     * Get sub-user id
     *
     * @return int
     */
    public function getSubId();

    /**
     * Set sub-user id
     *
     * @param int $id
     * @return void
     */
    public function setSubId($id);

    /**
     * Get order id
     *
     * @return int
     */
    public function getOrderId();

    /**
     * Set order id
     *
     * @param int $id
     * @return void
     */
    public function setOrderId($id);

    /**
     * Get grand total
     *
     * @return string
     */
    public function getGrandTotal();

    /**
     * Set grand total
     *
     * @param string $total
     * @return void
     */
    public function setGrandTotal($total);
}
