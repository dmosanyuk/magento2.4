<?php
namespace OnTap\CompanyAccount\Api\Data;

/**
 * Interface SubUserOrderSearchResultsInterface
 *
 * @package OnTap\CompanyAccount\Api\Data
 */
interface SubUserOrderSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get items
     *
     * @return \OnTap\CompanyAccount\Api\Data\SubUserInterface[]
     */
    public function getItems();

    /**
     * Set items
     *
     * @param \OnTap\CompanyAccount\Api\Data\SubUserInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
