<?php
namespace OnTap\CompanyAccount\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface SubRoleSearchResultsInterface
 *
 * @package OnTap\CompanyAccount\Api\Data
 */
interface SubRoleSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get items
     *
     * @return \OnTap\CompanyAccount\Api\Data\SubRoleInterface[]
     */
    public function getItems();

    /**
     * Set items
     *
     * @param \OnTap\CompanyAccount\Api\Data\SubRoleInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
