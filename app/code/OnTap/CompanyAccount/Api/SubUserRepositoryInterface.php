<?php
namespace OnTap\CompanyAccount\Api;

use OnTap\CompanyAccount\Api\Data\SubUserInterface;
use OnTap\CompanyAccount\Api\Data\SubUserSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Interface SubUserRepositoryInterface
 *
 * @package OnTap\CompanyAccount\Api
 */
interface SubUserRepositoryInterface
{
    /**
     * Save user information
     *
     * @param SubUserInterface $user
     *
     * @return SubUserInterface
     * @throws AlreadyExistsException
     */
    public function save(SubUserInterface $user);

    /**
     * Get sub user by id
     *
     * @param int $id
     * @return SubUserInterface
     * @throws NoSuchEntityException
     */
    public function getById($id);

    /**
     * Retrieve sub users matching the specified criteria
     *
     * @param SearchCriteriaInterface $criteria
     * @return SubUserSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $criteria);

    /**
     * Destroy sub user
     *
     * @param SubUserInterface $user
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(SubuserInterface $user);

    /**
     * Destroy sub user by id
     *
     * @param int $id
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function deleteById(int $id);

    /**
     * Get quote by sub-user
     *
     * @param int|SubUserInterface $subUser
     * @return null|\Magento\Quote\Api\Data\CartInterface
     */
    public function getQuoteBySubUser($subUser);

    /**
     * Get sub-user list by role
     *
     * @param int $roleId
     *
     * @return bool|\OnTap\CompanyAccount\Model\ResourceModel\SubUser\Collection
     */
    public function getByRole($roleId);

    /**
     * Validate unique email for sub-user
     *
     * @param int $customerId
     * @param string $email
     * @param int|null $subId
     *
     * @return void
     * @throws AlreadyExistsException
     */
    public function validateUniqueSubMail($customerId, $email, $subId = null);
}
