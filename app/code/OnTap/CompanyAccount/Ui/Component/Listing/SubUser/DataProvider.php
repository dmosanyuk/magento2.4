<?php
namespace OnTap\CompanyAccount\Ui\Component\Listing\SubUser;

use OnTap\CompanyAccount\Model\ResourceModel\SubUser\Grid\CollectionFactory;
use Magento\Framework\Api\Filter;

/**
 * Class DataProvider
 *
 * @package OnTap\CompanyAccount\Ui\Component\Listing\Role
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * DataProvider constructor.
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param array $meta
     * @param array $data
     * @param CollectionFactory $collectionFactory
     * @param \Magento\Framework\App\RequestInterface $request
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        \Magento\Framework\App\RequestInterface $request,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        $this->request = $request;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get roles data by company user
     *
     * @return array
     */
    public function getData(): array
    {
        $collection = $this->getCollection();
        $data['items'] = [];
        if ($this->request->getParam('parent_id')) {
            $collection->addFieldToFilter(
                [
                    \OnTap\CompanyAccount\Api\Data\SubUserInterface::CUSTOMER_ID,
                    \OnTap\CompanyAccount\Api\Data\SubUserInterface::CUSTOMER_ID
                ],
                [
                    ["eq" => (int)$this->request->getParam('parent_id')],
                    ["null" => true]
                ]
            );

        }
        $data = $collection->toArray();

        return $data;
    }

    /**
     * Add full text search filter to collection
     *
     * @param Filter $filter
     * @return void
     */
    public function addFilter(Filter $filter): void
    {
        if ($filter->getField() !== 'fulltext') {
            $this->collection->addFieldToFilter(
                $filter->getField(),
                [$filter->getConditionType() => $filter->getValue()]
            );
        } else {
            $value = trim($filter->getValue());
            $collection = $this->getCollection();
            $collection->addFieldToFilter(
                [
                    'sub_id',
                    'customer_id',
                    'sub_name',
                    'sub_email',
                    'sub_status',
                    'role_name',
                    'created_at',
                    'quote_id',
                    'parent_quote_id',
                    'quote_status'
                ],
                [
                    ['like' => "%{$value}%"],
                    ['like' => "%{$value}%"],
                    ['like' => "%{$value}%"],
                    ['like' => "%{$value}%"],
                    ['like' => "%{$value}%"],
                    ['like' => "%{$value}%"],
                    ['like' => "%{$value}%"],
                    ['like' => "%{$value}%"],
                    ['like' => "%{$value}%"],
                    ['like' => "%{$value}%"],
                ]
            );
            $this->collection = $collection;
        }
    }
}
