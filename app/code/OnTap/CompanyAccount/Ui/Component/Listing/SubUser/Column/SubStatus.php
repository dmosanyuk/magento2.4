<?php
declare(strict_types=1);
namespace OnTap\CompanyAccount\Ui\Component\Listing\SubUser\Column;

use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class SubStatus
 *
 * @package OnTap\CompanyAccount\Ui\Component\Listing\SubUser\Column
 */
class SubStatus extends Column
{
    /**
     * Prepare data source
     *
     * Change sub_status text
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$items) {
                $items['sub_status'] = $items['sub_status'] == "1" ? __("Enable") : __("Disabled");
            }
        }
        return $dataSource;
    }
}
