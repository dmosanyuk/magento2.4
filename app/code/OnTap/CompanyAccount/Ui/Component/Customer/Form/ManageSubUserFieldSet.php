<?php
namespace OnTap\CompanyAccount\Ui\Component\Customer\Form;

use OnTap\CompanyAccount\Helper\Data;
use Magento\Framework\View\Element\ComponentVisibilityInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;

/**
 * Class ManageSubUserFieldSet
 *
 * @package OnTap\CompanyAccount\Ui\Component\customer\Form
 */
class ManageSubUserFieldSet extends \Magento\Ui\Component\Form\Fieldset implements ComponentVisibilityInterface
{
    /**
     * @var Data
     */
    private $helper;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * ManageSubUserFieldSet constructor.
     *
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param ContextInterface $context
     * @param Data $helper
     * @param array $components
     * @param array $data
     */
    public function __construct(
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        ContextInterface $context,
        Data $helper,
        array $components = [],
        array $data = []
    ) {
        $this->customerRepository = $customerRepository;
        $this->context = $context;
        $this->helper = $helper;
        parent::__construct($context, $components, $data);
    }

    /**
     * Can show manage role tab in tabs or not
     *
     * Return true if customer is company account
     *
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function isComponentVisible(): bool
    {
        $customerId = $this->context->getRequestParam('id');
        if ($customerId) {
            $customer = $this->customerRepository->getById((int) $customerId);
            return $this->helper->isCompanyAccount($customer) && $this->helper->isEnable($customer->getWebsiteId());
        }
        return false;
    }
}
