<?php
/**
 * Copyright (c) On Tap Networks Limited.
 */
namespace OnTap\CompanyAccount\Ui\Component\Customer\Form;

use Magento\Customer\Api\CustomerRepositoryInterface;
use OnTap\CompanyAccount\Helper\Data;
use Magento\Framework\View\Element\ComponentVisibilityInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;

/**
 * Class CompanyInformationFieldSet
 *
 * @package OnTap\CompanyAccount\Ui\Component\Customer\Form
 */
class CompanyInformationFieldSet extends \Magento\Ui\Component\Form\Fieldset implements ComponentVisibilityInterface
{
    /**
     * @var Data
     */
    private $helper;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @param CustomerRepositoryInterface $customerRepository
     * @param ContextInterface $context
     * @param Data $helper
     * @param array $components
     * @param array $data
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        ContextInterface $context,
        Data $helper,
        array $components = [],
        array $data = []
    ) {
        $this->customerRepository = $customerRepository;
        $this->context = $context;
        $this->helper = $helper;
        parent::__construct($context, $components, $data);
    }

    /**
     * Check if tab should be visible
     *
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function isComponentVisible(): bool
    {
        $customerId = $this->context->getRequestParam('id');
        if ($customerId) {
            $customer = $this->customerRepository->getById((int) $customerId);
            return $this->helper->isCompanyAccount($customer) && $this->helper->isEnable($customer->getWebsiteId());
        }
        return false;
    }
}
