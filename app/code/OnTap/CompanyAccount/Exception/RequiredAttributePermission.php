<?php
namespace OnTap\CompanyAccount\Exception;

use Magento\Framework\Exception\LocalizedException;

/**
 * Class RequiredAttributePermission
 *
 * @package OnTap\CompanyAccount\Exception
 */
class RequiredAttributePermission extends LocalizedException
{
}
