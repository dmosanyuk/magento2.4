<?php
namespace OnTap\CompanyAccount\Exception;

use Magento\Framework\Exception\LocalizedException;

/**
 * Class EmptyInputException
 *
 * @package OnTap\CompanyAccount\Exception
 */
class CompareException extends LocalizedException
{
}
