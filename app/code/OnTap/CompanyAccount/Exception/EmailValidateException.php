<?php
namespace OnTap\CompanyAccount\Exception;

/**
 * Class EmailValidateException
 *
 * @package OnTap\CompanyAccount\Exception
 */
class EmailValidateException extends \Magento\Framework\Exception\LocalizedException
{

}
