<?php
namespace OnTap\CompanyAccount\Exception;

/**
 * Class CantDeleteAssignedRole
 *
 * @package OnTap\CompanyAccount\Exception
 */
class CantDeleteAssignedRole extends \Magento\Framework\Exception\LocalizedException
{

}
