<?php
namespace OnTap\CompanyAccount\ViewModel;

/**
 * Class RegistryData
 *
 * @package OnTap\CompanyAccount\ViewModel
 */
class RegistryData implements \Magento\Framework\View\Element\Block\ArgumentInterface
{
    /**
     * @var \Magento\Shipping\Helper\Data
     */
    private $shippingHelper;

    /**
     * @var \Magento\Tax\Helper\Data
     */
    private $taxHelper;

    /**
     * RegistryData constructor.
     *
     * @param \Magento\Shipping\Helper\Data $shippingHelper
     * @param \Magento\Tax\Helper\Data $taxHelper
     */
    public function __construct(
        \Magento\Shipping\Helper\Data $shippingHelper,
        \Magento\Tax\Helper\Data $taxHelper
    ) {
        $this->shippingHelper = $shippingHelper;
        $this->taxHelper = $taxHelper;
    }

    /**
     * Get shipping helper
     *
     * @return \Magento\Shipping\Helper\Data
     */
    public function getShippingHelper()
    {
        return $this->shippingHelper;
    }

    /**
     * Get tax helper object
     *
     * @return \Magento\Tax\Helper\Data
     */
    public function getTaxHelper()
    {
        return $this->taxHelper;
    }
}
