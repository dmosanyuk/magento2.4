<?php
/**
 * Copyright (c) On Tap Networks Limited.
 */

namespace OnTap\CompanyAccount\Block\Company;

use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\View\Element\Template;

/**
 * Class Index
 *
 * @package OnTap\CompanyAccount\Block\Company
 */
class Index extends Template
{
    /**
     * @var CustomerSession
     */
    protected $customerSession;

    protected $storeManager;

    /**
     * Index constructor.
     *
     * @param Template\Context $context
     * @param CustomerSession $customerSession
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        CustomerSession $customerSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->customerSession = $customerSession;
        $this->storeManager = $storeManager;
    }

    /**
     * Prepare render layout
     *
     * @return void
     */

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $this->pageConfig->getTitle()->set(__('Company Information'));

        $breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
        $breadcrumbs->addCrumb('Dashboard', [
            'label' => __('Dashboard'),
            'title' => __('Dashboard'),
            'link' => '/customer/account'
        ]);
        $breadcrumbs->addCrumb('Company', [
            'label' => __('Company'),
            'title' => __('Company')
        ]);
        $breadcrumbs->addCrumb('Company Information', [
            'label' => __('Company Information'),
            'title' => __('Company Information')
        ]);

    }


    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->customerSession->getCustomer()->getCompanyName();
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getLogo()
    {
        return $this->getMediaUrl() . 'ontap/customer' . $this->customerSession->getCustomer()->getCompanyLogo();
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getMediaUrl()
    {
        return $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }
}
