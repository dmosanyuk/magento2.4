<?php

namespace OnTap\CompanyAccount\Block\Adminhtml\Order;

use OnTap\CompanyAccount\Block\Sales\SubUserInfoHelper;
use Magento\Backend\Block\Template;
use Magento\Sales\Api\CreditmemoRepositoryInterface;

/**
 * Class CreditMemoSubInfo
 *
 * @package OnTap\CompanyAccount\Block\Adminhtml\Order
 */
class CreditMemoSubInfo extends Template
{
    /**
     * @var SubUserInfoHelper
     */
    private $subUserInfoHelper;

    /**
     * @var CreditmemoRepositoryInterface
     */
    private $creditMemoRepository;

    /**
     * CreditMemoSubInfo constructor.
     *
     * @param SubUserInfoHelper $subUserInfoHelper
     * @param Template\Context $context
     * @param CreditmemoRepositoryInterface $creditMemoRepository
     * @param array $data
     */
    public function __construct(
        SubUserInfoHelper $subUserInfoHelper,
        Template\Context $context,
        CreditmemoRepositoryInterface $creditMemoRepository,
        array $data = []
    ) {
        $this->subUserInfoHelper = $subUserInfoHelper;
        $this->creditMemoRepository = $creditMemoRepository;
        parent::__construct($context, $data);
    }

    /**
     * Get SubUser information
     *
     * @return bool|\OnTap\CompanyAccount\Api\Data\SubUserOrderInterface
     */
    public function getSubUserInfo()
    {
        $creId = $this->getRequest()->getParam('creditmemo_id');
        try {
            $invoice = $this->creditMemoRepository->get($creId);
            return $this->subUserInfoHelper->getSubUserInfo($invoice->getOrderId());
        } catch (\Exception $e) {
            return false;
        }
    }
}
