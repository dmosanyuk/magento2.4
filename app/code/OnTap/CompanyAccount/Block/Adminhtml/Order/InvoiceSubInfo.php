<?php

namespace OnTap\CompanyAccount\Block\Adminhtml\Order;

use OnTap\CompanyAccount\Block\Sales\SubUserInfoHelper;
use Magento\Backend\Block\Template;
use Magento\Sales\Api\InvoiceRepositoryInterface;

/**
 * Class InvoiceSubInfo
 *
 * @package OnTap\CompanyAccount\Block\Adminhtml\Order
 */
class InvoiceSubInfo extends Template
{
    /**
     * @var SubUserInfoHelper
     */
    private $subUserInfoHelper;

    /**
     * @var InvoiceRepositoryInterface
     */
    private $invoiceRepository;

    /**
     * InvoiceSubInfo constructor.
     *
     * @param SubUserInfoHelper $subUserInfoHelper
     * @param Template\Context $context
     * @param InvoiceRepositoryInterface $invoiceRepository
     * @param array $data
     */
    public function __construct(
        SubUserInfoHelper $subUserInfoHelper,
        Template\Context $context,
        InvoiceRepositoryInterface $invoiceRepository,
        array $data = []
    ) {
        $this->subUserInfoHelper = $subUserInfoHelper;
        $this->invoiceRepository = $invoiceRepository;
        parent::__construct($context, $data);
    }

    /**
     * Get SubUser information
     *
     * @return bool|\OnTap\CompanyAccount\Api\Data\SubUserOrderInterface
     */
    public function getSubUserInfo()
    {
        $invoiceId = $this->getRequest()->getParam('invoice_id');
        try {
            $invoice = $this->invoiceRepository->get($invoiceId);
            return $this->subUserInfoHelper->getSubUserInfo($invoice->getOrderId());
        } catch (\Exception $e) {
            return false;
        }
    }
}
