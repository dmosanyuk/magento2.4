<?php

namespace OnTap\CompanyAccount\Block\Adminhtml\Order;

use Magento\Backend\Block\Template;
use OnTap\CompanyAccount\Block\Sales\SubUserInfoHelper;

/**
 * Class SubInfo
 *
 * @package OnTap\CompanyAccount\Block\Adminhtml\Order
 */
class SubInfo extends Template
{
    /**
     * @var SubUserInfoHelper
     */
    private $subUserInfoHelper;

    /**
     * SubInfo constructor.
     *
     * @param SubUserInfoHelper $subUserInfoHelper
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        SubUserInfoHelper $subUserInfoHelper,
        Template\Context $context,
        array $data = []
    ) {
        $this->subUserInfoHelper = $subUserInfoHelper;
        parent::__construct($context, $data);
    }

    /**
     * Get SubUser information
     *
     * @return bool|\OnTap\CompanyAccount\Api\Data\SubUserOrderInterface
     */
    public function getSubUserInfo()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        return $this->subUserInfoHelper->getSubUserInfo($orderId);
    }
}
