<?php

namespace OnTap\CompanyAccount\Block\Adminhtml\Order;

use OnTap\CompanyAccount\Block\Sales\SubUserInfoHelper;
use Magento\Backend\Block\Template;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\ShipmentRepositoryInterface;

/**
 * Class ShipmentSubInfo
 *
 * @package OnTap\CompanyAccount\Block\Adminhtml\Order
 */
class ShipmentSubInfo extends Template
{
    /**
     * @var SubUserInfoHelper
     */
    private $subUserInfoHelper;

    /**
     * @var ShipmentRepositoryInterface
     */
    private $shipmentRepository;

    /**
     * ShipmentSubInfo constructor.
     *
     * @param SubUserInfoHelper $subUserInfoHelper
     * @param Template\Context $context
     * @param ShipmentRepositoryInterface $shipmentRepository
     * @param array $data
     */
    public function __construct(
        SubUserInfoHelper $subUserInfoHelper,
        Template\Context $context,
        ShipmentRepositoryInterface $shipmentRepository,
        array $data = []
    ) {
        $this->subUserInfoHelper = $subUserInfoHelper;
        $this->shipmentRepository = $shipmentRepository;
        parent::__construct($context, $data);
    }

    /**
     * Get SubUser information
     *
     * @return bool|\OnTap\CompanyAccount\Api\Data\SubUserOrderInterface
     */
    public function getSubUserInfo()
    {
        $shipmentId = $this->getRequest()->getParam('shipment_id');
        try {
            $shipment = $this->shipmentRepository->get($shipmentId);
            return $this->subUserInfoHelper->getSubUserInfo($shipment->getOrderId());
        } catch (NoSuchEntityException $e) {
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }
}
