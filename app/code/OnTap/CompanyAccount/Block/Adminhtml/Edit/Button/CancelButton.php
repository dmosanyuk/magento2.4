<?php
namespace OnTap\CompanyAccount\Block\Adminhtml\Edit\Button;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class CancelButton
 *
 * @package OnTap\CompanyAccount\Block\Adminhtml\Edit\Role
 */
class CancelButton implements ButtonProviderInterface
{
    /**
     * @var string
     */
    protected $targetName;

    /**
     * Get Cancel button data
     *
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Cancel'),
            'on_click' => '',
            'data_attribute' => [
                'mage-init' => [
                    'Magento_Ui/js/form/button-adapter' => [
                        'actions' => [
                            [
                                'targetName' => $this->targetName,
                                'actionName' => 'closeModal'
                            ],
                        ],
                    ],
                ],
            ],
            'sort_order' => 0
        ];
    }
}
