<?php
declare(strict_types=1);
namespace OnTap\CompanyAccount\Block\Adminhtml\Edit\Role;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use OnTap\CompanyAccount\Ui\Component\Listing\Role\Column\Actions;

/**
 * Delete button on edit customer address form
 */
class DeleteButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * Get delete button data.
     *
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getButtonData()
    {
        $data = [];
        if ($this->getRoleId()) {
            $data = [
                'label' => __('Delete'),
                'on_click' => '',
                'data_attribute' => [
                    'mage-init' => [
                        'Magento_Ui/js/form/button-adapter' => [
                            'actions' => [
                                [
                                    'targetName' => 'ontap_companyaccount_customer_listroles_form.'
                                        . 'ontap_companyaccount_customer_listroles_form',
                                    'actionName' => 'deleteRole',
                                    'params' => [
                                        $this->getDeleteUrl(),
                                    ],

                                ]
                            ],
                        ],
                    ],
                ],
                'sort_order' => 20
            ];
        }
        return $data;
    }

    /**
     * Get delete button url.
     *
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getDeleteUrl(): string
    {
        return $this->getUrl(
            Actions::CUSTOMER_ROLES_PATH_DELETE,
            ['customer_id' => $this->getCustomerId(), 'id' => $this->getRoleId()]
        );
    }
}
