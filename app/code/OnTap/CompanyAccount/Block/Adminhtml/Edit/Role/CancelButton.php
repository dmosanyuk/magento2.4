<?php
namespace OnTap\CompanyAccount\Block\Adminhtml\Edit\Role;

/**
 * Class CancelButton
 *
 * @package OnTap\CompanyAccount\Block\Adminhtml\Edit\Role
 */
class CancelButton extends \OnTap\CompanyAccount\Block\Adminhtml\Edit\Button\CancelButton
{
    /**
     * CancelButton constructor.
     */
    public function __construct()
    {
        $this->targetName = 'customer_form.areas.ontap_company_account_manage_role.'
            . 'ontap_company_account_manage_role.'
            . 'ontap_companyaccount_customer_listroles_update_modal';
    }
}
