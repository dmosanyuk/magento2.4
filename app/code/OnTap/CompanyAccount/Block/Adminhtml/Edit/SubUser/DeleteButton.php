<?php
declare(strict_types=1);
namespace OnTap\CompanyAccount\Block\Adminhtml\Edit\SubUser;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use OnTap\CompanyAccount\Ui\Component\Listing\SubUser\Column\Actions;

/**
 * Delete button on edit customer address form
 */
class DeleteButton extends GenericButton implements ButtonProviderInterface
{

    /**
     * Get delete button data.
     *
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getButtonData()
    {
        $data = [];
        if ($this->getSubId()) {
            $data = [
                'label' => __('Delete'),
                'on_click' => '',
                'data_attribute' => [
                    'mage-init' => [
                        'Magento_Ui/js/form/button-adapter' => [
                            'actions' => [
                                [
                                    'targetName' => 'ontap_companyaccount_customer_subuser_form.'
                                        . 'ontap_companyaccount_customer_subuser_form',
                                    'actionName' => 'deleteSubUser',
                                    'params' => [
                                        $this->getDeleteUrl(),
                                    ],

                                ]
                            ],
                        ],
                    ],
                ],
                'sort_order' => 20
            ];
        }
        return $data;
    }

    /**
     * Get delete button url.
     *
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getDeleteUrl(): string
    {
        return $this->getUrl(
            Actions::CUSTOMER_SUB_USER_PATH_DELETE,
            ['customer_id' => $this->getCustomerId(), 'id' => $this->getSubId()]
        );
    }
}
