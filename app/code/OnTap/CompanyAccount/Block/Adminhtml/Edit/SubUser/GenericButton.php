<?php
declare(strict_types=1);
namespace OnTap\CompanyAccount\Block\Adminhtml\Edit\SubUser;

use OnTap\CompanyAccount\Api\SubUserRepositoryInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\UrlInterface;

/**
 * Class for common code for buttons on the create/edit address form
 */
class GenericButton
{
    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var SubUserRepositoryInterface
     */
    private $userRepository;

    /**
     * @param UrlInterface $urlBuilder
     * @param RequestInterface $request
     * @param SubUserRepositoryInterface $userRepository
     */
    public function __construct(
        UrlInterface $urlBuilder,
        RequestInterface $request,
        SubUserRepositoryInterface $userRepository
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->request = $request;
        $this->userRepository = $userRepository;
    }

    /**
     * Get sub id
     *
     * @return int
     */
    public function getSubId()
    {
        return (int)$this->request->getParam('sub_id');
    }

    /**
     * Get customer id.
     *
     * @return int|null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCustomerId()
    {
        $userId = (int)$this->request->getParam('sub_id');

        $user = $this->userRepository->getById($userId);

        return $user->getCompanyCustomerId() ?: null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param string $route
     * @param array $params
     * @return  string
     */
    public function getUrl($route = '', array $params = []): string
    {
        return $this->urlBuilder->getUrl($route, $params);
    }
}
