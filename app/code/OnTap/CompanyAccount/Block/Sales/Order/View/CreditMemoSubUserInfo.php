<?php
namespace OnTap\CompanyAccount\Block\Sales\Order\View;

use OnTap\CompanyAccount\Block\Sales\SubUserInfoHelper;
use Magento\Framework\View\Element\Template;
use Magento\Sales\Api\CreditmemoRepositoryInterface;

/**
 * Class CreditMemoSubUserInfo
 *
 * @package OnTap\CompanyAccount\Block\Sales\Order\View
 */
class CreditMemoSubUserInfo extends Template
{
    /**
     * @var SubUserInfoHelper
     */
    private $subUserInfoHelper;

    /**
     * @var CreditmemoRepositoryInterface
     */
    private $creditMemoRepository;

    /**
     * CreditMemoSubUserInfo constructor.
     *
     * @param SubUserInfoHelper $subUserInfoHelper
     * @param CreditmemoRepositoryInterface $creditMemoRepository
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        SubUserInfoHelper $subUserInfoHelper,
        CreditmemoRepositoryInterface $creditMemoRepository,
        Template\Context $context,
        array $data = []
    ) {
        $this->subUserInfoHelper = $subUserInfoHelper;
        $this->creditMemoRepository = $creditMemoRepository;
        parent::__construct($context, $data);
    }

    /**
     * Get SubUser information
     *
     * @return bool|\OnTap\CompanyAccount\Api\Data\SubUserInterface
     */
    public function getSubUserInfo()
    {
        $creId = $this->getRequest()->getParam('creditmemo_id');
        $orderId = $this->getRequest()->getParam('order_id');
        try {
            if (!$orderId) {
                $invoice = $this->creditMemoRepository->get($creId);
                $orderId = $invoice->getOrderId();
            }
            return $this->subUserInfoHelper->getSubUserInfo($orderId);
        } catch (\Exception $e) {
            return false;
        }
    }
}
