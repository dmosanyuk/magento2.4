<?php
namespace OnTap\CompanyAccount\Block\Sales\Order\View;

use OnTap\CompanyAccount\Block\Sales\SubUserInfoHelper;
use Magento\Framework\View\Element\Template;

/**
 * Class SubUserInfo
 *
 * @package OnTap\CompanyAccount\Block\Sales\Order\View
 */
class SubUserInfo extends Template
{
    /**
     * @var SubUserInfoHelper
     */
    private $subUserInfoHelper;

    /**
     * SubInfo constructor.
     *
     * @param SubUserInfoHelper $subUserInfoHelper
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        SubUserInfoHelper $subUserInfoHelper,
        Template\Context $context,
        array $data = []
    ) {
        $this->subUserInfoHelper = $subUserInfoHelper;
        parent::__construct($context, $data);
    }

    /**
     * Get SubUser information
     *
     * @return bool|\OnTap\CompanyAccount\Api\Data\SubUserInterface
     */
    public function getSubUserInfo()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        return $this->subUserInfoHelper->getSubUserInfo($orderId);
    }
}
