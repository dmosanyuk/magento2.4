<?php
namespace OnTap\CompanyAccount\Block\Sales\Order\View;

use OnTap\CompanyAccount\Block\Sales\SubUserInfoHelper;
use Magento\Framework\View\Element\Template;
use Magento\Sales\Api\InvoiceRepositoryInterface;

/**
 * Class InvoiceSubUserInfo
 *
 * @package OnTap\CompanyAccount\Block\Sales\Order\View
 */
class InvoiceSubUserInfo extends Template
{
    /**
     * @var SubUserInfoHelper
     */
    private $subUserInfoHelper;

    /**
     * @var InvoiceRepositoryInterface
     */
    private $invoiceRepository;

    /**
     * InvoiceSubUserInfo constructor.
     *
     * @param SubUserInfoHelper $subUserInfoHelper
     * @param InvoiceRepositoryInterface $invoiceRepository
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        SubUserInfoHelper $subUserInfoHelper,
        InvoiceRepositoryInterface $invoiceRepository,
        Template\Context $context,
        array $data = []
    ) {
        $this->subUserInfoHelper = $subUserInfoHelper;
        $this->invoiceRepository = $invoiceRepository;
        parent::__construct($context, $data);
    }

    /**
     * Get SubUser information
     *
     * @return bool|\OnTap\CompanyAccount\Api\Data\SubUserInterface
     */
    public function getSubUserInfo()
    {
        $invoiceId = $this->getRequest()->getParam('invoice_id');
        $orderId = $this->getRequest()->getParam('order_id');
        try {
            if (!$orderId) {
                $invoice = $this->invoiceRepository->get($invoiceId);
                $orderId = $invoice->getOrderId();
            }
            return $this->subUserInfoHelper->getSubUserInfo($orderId);
        } catch (\Exception $e) {
            return false;
        }
    }
}
