<?php
namespace OnTap\CompanyAccount\Block\Sales\Order\View;

use OnTap\CompanyAccount\Block\Sales\SubUserInfoHelper;
use Magento\Framework\View\Element\Template;
use Magento\Sales\Api\ShipmentRepositoryInterface;

/**
 * Class ShipmentSubUserInfo
 *
 * @package OnTap\CompanyAccount\Block\Sales\Order\View
 */
class ShipmentSubUserInfo extends Template
{
    /**
     * @var SubUserInfoHelper
     */
    private $subUserInfoHelper;

    /**
     * @var ShipmentRepositoryInterface
     */
    private $shipmentRepository;

    /**
     * ShipmentSubUserInfo constructor.
     *
     * @param SubUserInfoHelper $subUserInfoHelper
     * @param ShipmentRepositoryInterface $shipmentRepository
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        SubUserInfoHelper $subUserInfoHelper,
        ShipmentRepositoryInterface $shipmentRepository,
        Template\Context $context,
        array $data = []
    ) {
        $this->subUserInfoHelper = $subUserInfoHelper;
        $this->shipmentRepository = $shipmentRepository;
        parent::__construct($context, $data);
    }

    /**
     * Get SubUser information
     *
     * @return bool|\OnTap\CompanyAccount\Api\Data\SubUserInterface
     */
    public function getSubUserInfo()
    {
        $shipmentId = $this->getRequest()->getParam('shipment_id');
        $orderId = $this->getRequest()->getParam('order_id');
        try {
            if (!$orderId) {
                $invoice = $this->shipmentRepository->get($shipmentId);
                $orderId = $invoice->getOrderId();
            }
            return $this->subUserInfoHelper->getSubUserInfo($orderId);
        } catch (\Exception $e) {
            return false;
        }
    }
}
