<?php
namespace OnTap\CompanyAccount\Block\Sales\Order;

use OnTap\CompanyAccount\Api\Data\SubUserInterface;
use OnTap\CompanyAccount\Block\Sales\SubUserInfoHelper;
use Magento\Framework\View\Element\Template;

/**
 * Class CreatedByCol
 *
 * @package OnTap\CompanyAccount\Block\Sales\Order
 */
class CreatedByCol extends Template
{
    /**
     * @var \Magento\Framework\Registry
     */
    private $reg;

    /**
     * @var SubUserInfoHelper
     */
    private $subUserInfoHelper;

    /**
     * CreatedByCol constructor.
     *
     * @param \Magento\Framework\Registry $reg
     * @param SubUserInfoHelper $subUserInfoHelper
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Registry $reg,
        SubUserInfoHelper $subUserInfoHelper,
        Template\Context $context,
        array $data = []
    ) {
        $this->reg = $reg;
        $this->subUserInfoHelper = $subUserInfoHelper;
        parent::__construct($context, $data);
    }

    /**
     * Get created sub-user name
     *
     * @return string
     */
    public function getCreatedBy()
    {
        $order = $this->getOrder();
        if ($order) {
            $subUserInfo = $this->subUserInfoHelper->getSubUserInfo($order->getEntityId());
            if ($subUserInfo) {
                return $subUserInfo[SubUserInterface::NAME];
            }
        }
        return '';
    }

    /**
     * Get registered order
     *
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    protected function getOrder()
    {
        return $this->reg->registry('ontap_reg_history_order');
    }
}
