<?php
namespace OnTap\CompanyAccount\Block\Sales;

use OnTap\CompanyAccount\Api\SubUserOrderRepositoryInterface;
use OnTap\CompanyAccount\Api\SubUserRepositoryInterface;
use OnTap\CompanyAccount\Helper\Data;

/**
 * Class SubUserInfoHelper
 *
 * @package OnTap\CompanyAccount\Block\Sales
 */
class SubUserInfoHelper
{
    /**
     * @var Data
     */
    private $helper;

    /**
     * @var SubUserOrderRepositoryInterface
     */
    private $userOrderRepository;

    /**
     * @var SubUserRepositoryInterface
     */
    private $subUserRepository;

    /**
     * SubUserInfoHelper constructor.
     *
     * @param Data $helper
     * @param SubUserOrderRepositoryInterface $userOrderRepository
     * @param SubUserRepositoryInterface $subUserRepository
     */
    public function __construct(
        Data $helper,
        SubUserOrderRepositoryInterface $userOrderRepository,
        SubUserRepositoryInterface $subUserRepository
    ) {
        $this->helper = $helper;
        $this->userOrderRepository = $userOrderRepository;
        $this->subUserRepository = $subUserRepository;
    }

    /**
     * Get SubUser information
     *
     * @param int $orderId
     * @return bool|array
     */
    public function getSubUserInfo($orderId)
    {
        try {
            if ($this->helper->isEnable()) {
                /** @var \OnTap\CompanyAccount\Api\Data\SubUserOrderInterface $subUserOrder */
                $subUserOrder = $this->userOrderRepository->getByOrderId($orderId);
                if ($subUserOrder) {
                    return $subUserOrder->getSubUserInfo();
                }
            }
        } catch (\Exception $exception) {
            $this->helper->getMessageManager()->addErrorMessage($exception->getMessage());
        }
        return false;
    }
}
