<?php
namespace OnTap\CompanyAccount\Block\SubUser;

use OnTap\CompanyAccount\Api\SubUserRepositoryInterface;
use OnTap\CompanyAccount\Model\ResourceModel\SubUser\CollectionFactory as Collection;
use Magento\Framework\View\Element\Template;

/**
 * Class Index
 *
 * @package OnTap\CompanyAccount\Block\SubUser
 */
class Index extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Customer\Helper\Session\CurrentCustomer
     */
    protected $currentCustomer;

    /**
     * @var SubUserRepositoryInterface
     */
    private $subUserRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $criteriaBuilder;

    /**
     * @var Collection
     */
    private $subUserCollection;

    /**
     * Index constructor.
     *
     * @param Template\Context $context
     * @param \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer
     * @param SubUserRepositoryInterface $subUserRepository
     * @param Collection $subUserCollection
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $criteriaBuilder
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer,
        SubUserRepositoryInterface $subUserRepository,
        Collection $subUserCollection,
        \Magento\Framework\Api\SearchCriteriaBuilder $criteriaBuilder,
        array $data = []
    ) {
        $this->currentCustomer = $currentCustomer;
        $this->subUserRepository = $subUserRepository;
        $this->criteriaBuilder = $criteriaBuilder;
        $this->subUserCollection = $subUserCollection;
        parent::__construct($context, $data);
    }

    /**
     * Manage sub-user constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $collection = $this->subUserCollection->create();

        $collection->addFieldToFilter('customer_id', $this->currentCustomer->getCustomerId())
        ->addOrder('sub_id', 'desc');
        $this->setItems($collection);
    }

    /**
     * Enter description here...
     *
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $pager = $this->getLayout()->createBlock(
            \Magento\Theme\Block\Html\Pager::class,
            'companyaccount.subuser.index'
        )->setCollection(
            $this->getItems()
        )->setPath('companyaccount/subuser/');
        $this->setChild('pager', $pager);
        $this->getItems()->load();

        $breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
        $breadcrumbs->addCrumb('Dashboard', [
            'label' => __('Dashboard'),
            'title' => __('Dashboard'),
            'link' => '/customer/account'
        ]);
        $breadcrumbs->addCrumb('Company', [
            'label' => __('Company'),
            'title' => __('Company')
        ]);
        $breadcrumbs->addCrumb('Manage Company Users', [
            'label' => __('Manage Company Users'),
            'title' => __('Manage Company Users')
        ]);

        return $this;
    }

    /**
     * Get edit link
     *
     * @param \OnTap\CompanyAccount\Api\Data\SubUserInterface $subUser
     * @return string
     */
    public function getEditUrl($subUser)
    {
        return $this->getUrl('companyaccount/subuser/edit', ['sub_id' => $subUser->getSubUserId()]);
    }

    /**
     * Get create url
     *
     * @return string
     */
    public function getCreateUrl()
    {
        return $this->getUrl('companyaccount/subuser/create');
    }

    /**
     * Get delete link
     *
     * @param \OnTap\CompanyAccount\Api\Data\SubUserInterface $subUser
     * @return string
     */
    public function getDeleteUrl($subUser)
    {
        return $this->getUrl('companyaccount/subuser/delete', ['sub_id' => $subUser->getSubUserId()]);
    }

    /**
     * Get reset password link
     *
     * @param \OnTap\CompanyAccount\Api\Data\SubUserInterface $subUser
     *
     * @return string
     */
    public function getResetPasswordUrl($subUser)
    {
        return $this->getUrl('companyaccount/subuser/resetpassword', ['sub_id' => $subUser->getSubUserId()]);
    }
}
