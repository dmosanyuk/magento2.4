<?php
namespace OnTap\CompanyAccount\Block\Role;

use OnTap\CompanyAccount\Api\Data\SubRoleInterfaceFactory;
use OnTap\CompanyAccount\Api\SubRoleRepositoryInterface;
use OnTap\CompanyAccount\Block\Adminhtml\Edit\Role\Permission;
use OnTap\CompanyAccount\Helper\Data;
use OnTap\CompanyAccount\Model\Config\Source\Permissions;
use Magento\Customer\Model\Session;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * Class Edit
 *
 * @package OnTap\CompanyAccount\Block\Role
 */
class Edit extends Template
{
    /**
     * @var \OnTap\CompanyAccount\Api\Data\SubRoleInterface
     */
    protected $role = null;

    /**
     * @var SubRoleRepositoryInterface
     */
    private $roleRepository;

    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var SubRoleInterfaceFactory
     */
    private $roleFactory;

    /**
     * @var Permissions
     */
    private $permissions;

    /**
     * @var Data
     */
    private $helper;

    /**
     * @var Permission
     */
    private $permission;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * Edit constructor.
     *
     * @param Template\Context $context
     * @param Session $customerSession
     * @param Permission $permission
     * @param SubRoleInterfaceFactory $roleFactory
     * @param SubRoleRepositoryInterface $roleRepository
     * @param Data $helper
     * @param SerializerInterface $serializer
     * @param Permissions $permissions
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Session $customerSession,
        Permission $permission,
        SubRoleInterfaceFactory $roleFactory,
        SubRoleRepositoryInterface $roleRepository,
        Data $helper,
        SerializerInterface $serializer,
        Permissions $permissions,
        array $data = []
    ) {
        $this->roleRepository = $roleRepository;
        $this->customerSession = $customerSession;
        $this->roleFactory = $roleFactory;
        $this->permissions = $permissions;
        $this->helper = $helper;
        $this->permission = $permission;
        $this->serializer = $serializer;
        parent::__construct($context, $data);
    }

    /**
     * Convert amount currency
     *
     * @param float $amount
     * @return float|string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function convertCurrency($amount)
    {
        if (empty($amount)) {
            return '';
        }
        return ($this->helper->convertCurrency($amount));
    }

    /**
     * Get serializer object
     *
     * @return SerializerInterface
     */
    public function getSerializer()
    {
        return $this->serializer;
    }

    /**
     * Get list rules
     *
     * @return array
     */
    public function getDataRules()
    {
        return $this->permission->getDataRules();
    }

    /**
     * Get permissions of role
     *
     * @return false|string[]
     * @throws NoSuchEntityException
     */
    public function getSelectedRules()
    {
        return $this->permission->getSelectedRules();
    }

    /**
     * Prepare render layout
     *
     * @return void
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $this->initRole();
        $this->pageConfig->getTitle()->set($this->getTitle());

        $breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
        $breadcrumbs->addCrumb('Dashboard', [
            'label' => __('Dashboard'),
            'title' => __('Dashboard'),
            'link' => '/customer/account'
        ]);
        $breadcrumbs->addCrumb('Company', [
            'label' => __('Company'),
            'title' => __('Company')
        ]);
        $breadcrumbs->addCrumb('Manage Roles and Permissions', [
            'label' => __('Manage Roles and Permissions'),
            'title' => __('Manage Roles and Permissions')
        ]);
        $breadcrumbs->addCrumb('Edit user', [
            'label' => $this->getTitle(),
            'title' => $this->getTitle()
        ]);


    }

    /**
     * Initialize role object
     *
     * @return void
     */
    public function initRole()
    {
        /** @var \Zend\Stdlib\Parameters $oldRoleData */
        $oldRoleData = $this->helper->getDataHelper()->getCoreSession()->getRoleFormData();
        if ($oldRoleData) {
            $this->role = $this->roleFactory->create();
            $this->role->setRoleName($oldRoleData->get('role_name'));
            if (is_array($oldRoleData->get('role_type'))) {
                $this->role->setRoleType(implode(',', $oldRoleData->get('role_type')));
            } else {
                $this->role->setRoleType($oldRoleData->get('role_type') ?? "");
            }
            $this->role->setMaxOrderAmount($oldRoleData->get('max_order_amount'));
            $this->role->setMaxOrderPerDay($oldRoleData->get('order_per_day'));
            $this->helper->getDataHelper()->getCoreSession()->unsRoleFormData();
        } elseif ($roleId = $this->getRequest()->getParam('role_id')) {
            try {
                $this->role = $this->roleRepository->getById($roleId);
                if ($this->getRole()->getCompanyAccount() != $this->customerSession->getCustomerId()) {
                    $this->role = null;
                }
            } catch (NoSuchEntityException $e) {
                $this->role = null;
            }
        }
        if ($this->role === null) {
            $this->role = $this->roleFactory->create();
        }
    }

    /**
     * Return the title, either editing an existing address, or adding a new one.
     *
     * @return string
     */
    public function getTitle()
    {
        if (!$this->getRole()->getRoleId()) {
            $title = __('Add New Role');
        } else {
            $title = __('Edit %1', $this->getRole()->getRoleName());
        }
        return $title;
    }

    /**
     * Return the Url for saving.
     *
     * @return string
     */
    public function getSaveUrl()
    {
        return $this->_urlBuilder->getUrl(
            'companyaccount/role/formPost',
            ['_secure' => true, 'role_id' => $this->getRole()->getRoleId()]
        );
    }

    /**
     * Get role
     *
     * @return \OnTap\CompanyAccount\Api\Data\SubRoleInterface
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Get previous url
     *
     * @return string
     */
    public function getBackUrl()
    {
        if ($this->getData('back_url')) {
            return $this->getData('back_url');
        }

        return $this->getUrl('companyaccount/role/');
    }

    /**
     * Get permissions html text
     *
     * @return string
     */
    public function getPermissionOptions()
    {
        $permissions = $this->permissions->toOptionArray();
        return $this->getSelectOptions($permissions);
    }

    /**
     * Get array of permission
     *
     * @return array
     */
    protected function toRolePermissionArray()
    {
        if ($this->role->getRoleType()) {
            return explode(',', $this->role->getRoleType());
        }
        return [];
    }

    /**
     * Get content of select option
     *
     * @param array $options
     * @return string
     */
    protected function getSelectOptions(array $options)
    {
        $htmlContent = '';
        $rolePermissions = $this->toRolePermissionArray();
        foreach ($options as $option) {
            $htmlContent .= '<option value="' . $option['value'] . '" ';
            if (in_array($option['value'], $rolePermissions)) {
                $htmlContent .= 'selected';
            }
            $htmlContent .= '>' . $option['label'] . '</option>';
        }
        return $htmlContent;
    }

    /**
     * Get max order amount
     *
     * @return mixed
     */
    public function getMaxOrderAmount()
    {
        return $this->helper->getMaxOrderAmount();
    }

    /**
     * Get max order per day
     *
     * @return mixed
     */
    public function getMaxOrderPerDay()
    {
        return $this->helper->getMaxOrderPerDay();
    }
}
