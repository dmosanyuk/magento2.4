<?php
namespace OnTap\CompanyAccount\Block\Role;

use OnTap\CompanyAccount\Api\SubRoleRepositoryInterface as RoleRepository;
use OnTap\CompanyAccount\Helper\CheckAssignedRole;
use OnTap\CompanyAccount\Helper\Data;
use OnTap\CompanyAccount\Model\ResourceModel\SubRole\CollectionFactory as Collection;
use Magento\Framework\View\Element\Template;

/**
 * Class Index
 *
 * @package OnTap\CompanyAccount\Block\Role
 */
class Index extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Customer\Helper\Session\CurrentCustomer
     */
    protected $currentCustomer;

    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * @var Collection
     */
    private $roleCollection;

    /**
     * @var CheckAssignedRole
     */
    private $checkAssignedRole;

    /**
     * @var Data
     */
    private $helper;

    /**
     * Index constructor.
     *
     * @param Template\Context $context
     * @param \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer
     * @param RoleRepository $roleRepository
     * @param Data $helper
     * @param CheckAssignedRole $checkAssignedRole
     * @param Collection $roleCollection
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer,
        RoleRepository $roleRepository,
        Data $helper,
        CheckAssignedRole $checkAssignedRole,
        Collection $roleCollection,
        array $data = []
    ) {
        $this->currentCustomer = $currentCustomer;
        $this->roleRepository = $roleRepository;
        $this->roleCollection = $roleCollection;
        $this->checkAssignedRole = $checkAssignedRole;
        $this->helper = $helper;
        parent::__construct($context, $data);
    }

    /**
     * Manage sub-user constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $collection = $this->roleCollection->create();

        $collection->addFieldToFilter(
            [
                'customer_id',
                'customer_id'
            ],
            [
                ['eq' => $this->currentCustomer->getCustomerId()],
                ["null" => true]
            ]
        )
        ->addOrder('role_id', 'desc');
        $this->setItems($collection);
    }

    /**
     * Convert amount currency
     *
     * @param float $amount
     * @return float|string
     */
    public function convertCurrency($amount)
    {
        if (empty($amount)) {
            return '';
        }
        return $this->helper->convertFormatCurrency($amount);
    }

    /**
     * Enter description here...
     *
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $pager = $this->getLayout()->createBlock(
            \Magento\Theme\Block\Html\Pager::class,
            'companyaccount.role.index'
        )->setCollection(
            $this->getItems()
        )->setPath('companyaccount/role/');
        $this->setChild('pager', $pager);
        $this->getItems()->load();

        $breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
        $breadcrumbs->addCrumb('Dashboard', [
            'label' => __('Dashboard'),
            'title' => __('Dashboard'),
            'link' => '/customer/account'
        ]);
        $breadcrumbs->addCrumb('Company', [
            'label' => __('Company'),
            'title' => __('Company')
        ]);
        $breadcrumbs->addCrumb('Manage Roles and Permissions', [
            'label' => __('Manage Roles and Permissions'),
            'title' => __('Manage Roles and Permissions')
        ]);


        return $this;
    }

    /**
     * Format permissions
     *
     * Remove basic and advanced role group val
     *
     * @param string $permissionString
     * @return string
     */
    public function formatPermission($permissionString)
    {
        $roleType = explode(',', $permissionString);
        $roleType = array_filter($roleType, function ($value) {
            return (int) $value > 0;
        });
        return implode(',', $roleType);
    }

    /**
     * Get edit link
     *
     * @param \OnTap\CompanyAccount\Api\Data\SubRoleInterface $role
     * @return string
     */
    public function getEditUrl($role)
    {
        return $this->getUrl('companyaccount/role/edit', ['role_id' => $role->getRoleId()]);
    }

    /**
     * Get create url
     *
     * @return string
     */
    public function getCreateUrl()
    {
        return $this->getUrl('companyaccount/role/create');
    }

    /**
     * Get delete link
     *
     * @param \OnTap\CompanyAccount\Api\Data\SubRoleInterface $role
     * @return string
     */
    public function getDeleteUrl($role)
    {
        return $this->getUrl('companyaccount/role/delete', ['role_id' => $role->getRoleId()]);
    }

    /**
     * Get max order amount
     *
     * @return mixed
     */
    public function getMaxOrderAmount()
    {
        return $this->helper->getMaxOrderAmount();
    }

    /**
     * Get max order per day
     *
     * @return mixed
     */
    public function getMaxOrderPerDay()
    {
        return $this->helper->getMaxOrderPerDay();
    }
}
