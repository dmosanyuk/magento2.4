<?php
namespace OnTap\CompanyAccount\Block\Customer\Tab;

use OnTap\CompanyAccount\Helper\Data;
use Magento\Customer\Block\Account\SortLinkInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\DefaultPathInterface;
use Magento\Framework\View\Element\Html\Link\Current;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class SubChangePassword
 *
 * @package OnTap\CompanyAccount\Block\Customer\Tab
 */
class SubChangePassword extends Current implements SortLinkInterface
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var Data
     */
    private $helper;

    /**
     * SubChangePassword constructor.
     *
     * @param Context $context
     * @param DefaultPathInterface $defaultPath
     * @param Data $helper
     * @param array $data
     */
    public function __construct(
        Context $context,
        DefaultPathInterface $defaultPath,
        Data $helper,
        array $data = []
    ) {
        $this->helper = $helper;
        $this->session = $this->helper->getCustomerSession();
        parent::__construct($context, $defaultPath, $data);
    }

    /**
     * Produce and return block's html output
     *
     * If logged in is sub-user will show this tab
     *
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function toHtml()
    {
        $subUser = $this->session->getSubUser();
        if ($subUser &&
            $this->helper->isEnable($this->helper->getStoreManager()->getWebsite()->getId())
        ) {
            return parent::toHtml();
        }
        return '';
    }

    /**
     * Get sort order for block.
     *
     * @return int
     */
    public function getSortOrder()
    {
        return $this->getData(self::SORT_ORDER);
    }
}
