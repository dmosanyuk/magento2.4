<?php
namespace OnTap\CompanyAccount\Observer;

use OnTap\CompanyAccount\Helper\PermissionsChecker;
use OnTap\CompanyAccount\Model\Config\Source\Permissions;
use Magento\Framework\Event\Observer;

/**
 * Class CustomerDashboard
 *
 * @package OnTap\CompanyAccount\Observer
 */
class CustomerDashboard implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var PermissionsChecker
     */
    private $checker;

    /**
     * CustomerDashboard constructor.
     *
     * @param PermissionsChecker $checker
     */
    public function __construct(
        PermissionsChecker $checker
    ) {
        $this->checker = $checker;
    }

    /**
     * @inheritDoc
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Framework\View\Layout $layout */
        $layout = $observer->getLayout();
        $currentHandles = $layout->getUpdate()->getHandles();
        if (!in_array('customer_account_index', $currentHandles)) {
            return $this;
        }
        $checker = $this->checker->isDenied(Permissions::VIEW_ACCOUNT_DASHBOARD);
        if ($checker) {
            $layout->getUpdate()->addHandle('no_access');
        }
        return $this;
    }
}
