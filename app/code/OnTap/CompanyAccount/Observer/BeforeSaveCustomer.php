<?php

namespace OnTap\CompanyAccount\Observer;

use OnTap\CompanyAccount\Controller\Adminhtml\Customer\SendActiveCompanyAccountEmail;
use OnTap\CompanyAccount\Controller\Adminhtml\Customer\SendDeactiveCompanyAccountEmail;
use OnTap\CompanyAccount\Helper\Data;
use OnTap\CompanyAccount\Model\ResourceModel\Customer;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Message\ManagerInterface;

/**
 * Class BeforeSaveCustomer
 *
 * @package OnTap\CompanyAccount\Observer
 */
class BeforeSaveCustomer implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var Data
     */
    private $helper;

    /**
     * @var \Magento\Framework\Registry
     */
    private $registry;

    /**
     * @var Customer
     */
    private $customerResource;

    /**
     * BeforeSaveCustomer constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository
     * @param \Magento\Framework\Registry $registry
     * @param Customer $customerResource
     * @param Data $helper
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\Registry $registry,
        Customer $customerResource,
        Data $helper,
        ManagerInterface $messageManager
    ) {
        $this->messageManager = $messageManager;
        $this->customerRepository = $customerRepository;
        $this->helper = $helper;
        $this->registry = $registry;
        $this->customerResource = $customerResource;
    }

    /**
     * Before save customer observer
     *
     * Before save will check if customer is company account then assign flag to send active
     * email after save.
     *
     * @param Observer $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Customer\Model\Backend\Customer $customer */
        $customer = $observer->getCustomer();

        $subUserResult = $this->customerResource
            ->validateUniqueCustomer($customer->getEmail(), (int) $customer->getWebsiteId());

        $this->registry->unregister('already_exists_email');
        if ($subUserResult) {
            $this->registry->register('already_exists_email', 1);
            throw new AlreadyExistsException(
                __('A user with the same email address already exists in an associated website.')
            );
        }

        /** Begin Check and set flag to send company account status notification after */
        if ($this->helper->isEnable($customer->getWebsiteId())) {
            $this->registry->unregister('ontap_send_mail');
            $newCompanyAccountStatus = (int) $customer->getData('ontap_is_company_account');
            if ($customer->getId()) {
                $currentCustomer = $this->customerRepository->getById($customer->getId());
                $currentCompanyAccountStatus = $currentCustomer->getCustomAttribute('ontap_is_company_account');
            } else {
                $currentCompanyAccountStatus = null;
            }

            if ($currentCompanyAccountStatus !== null) {
                $hasChanged = (int) $currentCompanyAccountStatus->getValue() !== $newCompanyAccountStatus;
                if ($hasChanged) {
                    if ($newCompanyAccountStatus) {
                        $this->registry->register('ontap_send_mail', 1);
                    } else {
                        $this->registry->register('ontap_send_mail', 0);
                    }
                }
            } else {
                if ($newCompanyAccountStatus) {
                    $this->registry->register('ontap_send_mail', 1);
                }
            }
        }
    }
}
