<?php
namespace OnTap\CompanyAccount\Observer\Sales\Order\Email;

use OnTap\CompanyAccount\Api\SubUserOrderRepositoryInterface;
use OnTap\CompanyAccount\Api\SubUserRepositoryInterface;
use OnTap\CompanyAccount\Helper\Data;

/**
 * Class Sender
 *
 * @package OnTap\CompanyAccount\Observer\Sales\Order\Email
 */
class Sender implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magento\Framework\Registry
     */
    private $registry;

    /**
     * @var Data
     */
    private $helper;

    /**
     * @var SubUserOrderRepositoryInterface
     */
    private $userOrderRepository;

    /**
     * @var SubUserRepositoryInterface
     */
    private $subUserRepository;

    /**
     * Sender constructor.
     *
     * @param \Magento\Framework\Registry $registry
     * @param SubUserRepositoryInterface $subUserRepository
     * @param SubUserOrderRepositoryInterface $userOrderRepository
     * @param Data $helper
     */
    public function __construct(
        \Magento\Framework\Registry $registry,
        SubUserRepositoryInterface $subUserRepository,
        SubUserOrderRepositoryInterface $userOrderRepository,
        Data $helper
    ) {
        $this->registry = $registry;
        $this->helper = $helper;
        $this->userOrderRepository = $userOrderRepository;
        $this->subUserRepository = $subUserRepository;
    }

    /**
     * Registry sub-user email for notify to registered sub-user
     *
     * Working when module is enable and order was placed by sub-user
     *
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        $transportObject = $observer->getData('transportObject');
        $this->registry->unregister('ontap_is_send_mail_to_sub_user');
        if ($this->helper->isEnable()) {
            /** @var \Magento\Sales\Model\Order $order */
            $order = $transportObject->getData('order');
            $userOrder = $this->userOrderRepository->getByOrderId($order->getId());
            if ($userOrder) {
                $subUser = $this->subUserRepository->getById($userOrder->getSubId());
                if ($subUser) {
                    $this->registry->register('ontap_is_send_mail_to_sub_user', $subUser);
                }
            }
        }
    }
}
