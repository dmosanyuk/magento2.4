<?php
namespace OnTap\CompanyAccount\Observer;

use OnTap\CompanyAccount\Api\Data\SubUserInterface;
use OnTap\CompanyAccount\Api\SubUserRepositoryInterface;
use OnTap\CompanyAccount\Helper\Data;
use Magento\Customer\Model\Session;
use Magento\Framework\Event\Observer;
use Magento\Framework\Exception\CouldNotSaveException;
use Psr\Log\LoggerInterface;

/**
 * Class OrderPlaced
 *
 * @package OnTap\CompanyAccount\Observer
 */
class OrderPlaced implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var Data
     */
    private $helper;

    /**
     * @var \OnTap\CompanyAccount\Api\Data\SubUserOrderInterfaceFactory
     */
    private $userOrderFactory;

    /**
     * @var \OnTap\CompanyAccount\Api\SubUserOrderRepositoryInterface
     */
    private $userOrderRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SubUserRepositoryInterface
     */
    private $subUserRepository;

    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    private $serializer;

    /**
     * OrderPlaced constructor.
     *
     * @param LoggerInterface $logger
     * @param \Magento\Framework\Serialize\Serializer\Json $serializer
     * @param SubUserRepositoryInterface $subUserRepository
     * @param \OnTap\CompanyAccount\Api\Data\SubUserOrderInterfaceFactory $userOrderFactory
     * @param \OnTap\CompanyAccount\Api\SubUserOrderRepositoryInterface $userOrderRepository
     * @param Data $helper
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Serialize\Serializer\Json $serializer,
        SubUserRepositoryInterface $subUserRepository,
        \OnTap\CompanyAccount\Api\Data\SubUserOrderInterfaceFactory $userOrderFactory,
        \OnTap\CompanyAccount\Api\SubUserOrderRepositoryInterface $userOrderRepository,
        Data $helper
    ) {
        $this->helper = $helper;
        $this->customerSession = $this->helper->getCustomerSession();
        $this->userOrderFactory = $userOrderFactory;
        $this->userOrderRepository = $userOrderRepository;
        $this->logger = $logger;
        $this->subUserRepository = $subUserRepository;
        $this->serializer = $serializer;
    }

    /**
     * @inheritDoc
     */
    public function execute(Observer $observer)
    {
        /** @var \OnTap\CompanyAccount\Api\Data\SubUserInterface $subUser */
        $subUser = $this->customerSession->getSubUser();
        try {
            if ($this->helper->isEnable() && $subUser) {
                /** @var \Magento\Sales\Api\Data\OrderInterface $order */
                $order = $observer->getOrder();
                /** @var \OnTap\CompanyAccount\Api\Data\SubUserOrderInterface $userOrder */
                $userOrder = $this->userOrderFactory->create();
                $userOrder->setSubId($subUser->getSubUserId());
                $userOrder->setOrderId($order->getEntityId());
                $userOrder->setGrandTotal($order->getBaseGrandTotal());
                $subUser = $this->subUserRepository->getById($subUser->getSubUserId());
                {
                    $subUserInfo[SubUserInterface::NAME] = $subUser->getSubUserName();
                    $subUserInfo[SubUserInterface::EMAIL] = $subUser->getSubUserEmail();
                    $subUserInfo['role_name'] = $subUser->getData('role_name');
                }
                $userOrder->setSubUserInfo(
                    $this->serializer->serialize($subUserInfo)
                );
                $this->userOrderRepository->save($userOrder);
            }
        } catch (CouldNotSaveException $e) {
            $this->addErrorMsg($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            $this->addErrorMsg(__('Something went wrong. Please try again later.'));
        }
    }

    /**
     * Add error message
     *
     * @param string $text
     */
    protected function addErrorMsg($text)
    {
        $this->helper->getMessageManager()->addErrorMessage($text);
    }
}
