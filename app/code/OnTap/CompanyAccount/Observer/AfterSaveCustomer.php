<?php

namespace OnTap\CompanyAccount\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Message\ManagerInterface;

/**
 * Class BeforeSaveCustomer
 *
 * @package OnTap\CompanyAccount\Observer
 */
class AfterSaveCustomer implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \OnTap\CompanyAccount\Helper\EmailHelper
     */
    private $emailHelper;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * @var \Magento\Framework\Registry
     */
    private $registry;

    /**
     * AfterSaveCustomer constructor.
     *
     * @param \OnTap\CompanyAccount\Helper\EmailHelper $emailHelper
     * @param \Magento\Framework\Registry $registry
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        \OnTap\CompanyAccount\Helper\EmailHelper $emailHelper,
        \Magento\Framework\Registry $registry,
        ManagerInterface $messageManager
    ) {
        $this->emailHelper = $emailHelper;
        $this->messageManager = $messageManager;
        $this->registry = $registry;
    }

    /**
     * Before save customer observer
     *
     * Get send mail from before save, check and send mail
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Customer\Model\Backend\Customer $customer */
        $customer = $observer->getCustomer();
        $isSendActiveCaEmail = $this->registry->registry('ontap_send_mail');
        if ($isSendActiveCaEmail !== null) {
            try {
                if ($isSendActiveCaEmail) {
                    $this->emailHelper->sendActiveCompanyAccountToCustomer($customer);
                } else {
                    $this->emailHelper->sendDeactiveCompanyAccountToCustomer($customer);
                }
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('Oops.. Something went wrong when we send mail to customer.'));
            }
        }
    }
}
