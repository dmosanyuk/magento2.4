<?php
/**
 * Copyright © On Tap Networks Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OnTap\CompanyAccount\Registry;

class SocialLogin
{
    /**
     * @var \Hybrid_User_Profile|null
     */
    protected $userProfile;

    /**
     * Retrieve real user profile
     *
     * @return \Hybrid_User_Profile|null
     */
    public function getUserProfile()
    {
        return $this->userProfile;
    }

    /**
     * Set real user profile
     *
     * @param \Hybrid_User_Profile $userProfile
     */
    public function setUserProfile(\Hybrid_User_Profile $userProfile)
    {
        $this->userProfile = $userProfile;
    }
}
